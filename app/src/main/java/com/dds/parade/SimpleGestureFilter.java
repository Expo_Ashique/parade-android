package com.dds.parade;

import android.app.Activity;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Created by nihal on 1/21/2016.
 */
public class SimpleGestureFilter extends
        GestureDetector.SimpleOnGestureListener {
    public final static int SWIPE_UP= 1;
    public final static int SWIPE_DOWN= 2;
    public final static int SWIPE_LEFT= 3;
    public final static int SWIPE_RIGHT = 4;
    public final static int MODE_TRANSPARENT = 0;
    public final static int MODE_SOLID= 1;
    public final static int MODE_DYNAMIC= 2;
    private final static int ACTION_FAKE = -13; //just an unlikely number
    private int swipe_Min_Distance = 100;
    private int swipe_Max_Distance = 350;
    private int swipe_Min_Velocity = 100;
    private int mode = MODE_DYNAMIC;
    private boolean running = true;
    private boolean tapIndicator = false;
    private Activity context;
    private GestureDetector detector;
    private SimpleGestureListener listener;
    public SimpleGestureFilter(Activity context,SimpleGestureListener sgl) {

        this.context = context;
        this.detector = new GestureDetector(context, this);
        this.listener = sgl;
    }
    public void onTouchEvent(MotionEvent event){

        if(!this.running)
            return;

        boolean result = this.detector.onTouchEvent(event);

        if(this.mode == MODE_SOLID)
            event.setAction(MotionEvent.ACTION_CANCEL);
        else if (this.mode == MODE_DYNAMIC) {

            if(event.getAction() == ACTION_FAKE)
                event.setAction(MotionEvent.ACTION_UP);
            else if (result)
                event.setAction(MotionEvent.ACTION_CANCEL);
            else if(this.tapIndicator){
                event.setAction(MotionEvent.ACTION_DOWN);
                this.tapIndicator = false;
            }

        }
        //else just do nothing, it's Transparent
    }
    public void setMode(int m){
        this.mode = m;
    }
    public int getMode(){
        return this.mode;
    }
    public void setEnabled(boolean status){
        this.running = status;
    }
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                           float velocityY) {

        try {
            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) {
                return false;
            }
            // right to left swipe
            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) { // right to left
                this.listener.onSwipe(SWIPE_LEFT);



            }if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) { // right to left
                this.listener.onSwipe(SWIPE_RIGHT);



            } else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) { //    bottom to up
                this.listener.onSwipe(SWIPE_UP);


            }

        } catch (Exception e) {

        }
        return false;
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        this.tapIndicator = true;
        return false;
    }
    @Override
    public boolean onDoubleTap(MotionEvent arg0) {
        this.listener.onDoubleTap();;
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent arg0) {
        return true;
    }
    @Override
    public boolean onSingleTapConfirmed(MotionEvent arg0) {

        if(this.mode == MODE_DYNAMIC){// we owe an ACTION_UP, so we fake an
            arg0.setAction(ACTION_FAKE);//action which will be converted to an ACTION_UP later.
            this.context.dispatchTouchEvent(arg0);
        }
        return false;
    }

    public interface SimpleGestureListener{
        void onSwipe(int direction);
        void onDoubleTap();
    }

}