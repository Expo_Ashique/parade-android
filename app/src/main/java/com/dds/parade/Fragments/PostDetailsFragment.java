package com.dds.parade.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.dds.parade.Activity.AddToClipListActivity;
import com.dds.parade.Activity.HomeActivity;
import com.dds.parade.OnSwipeTouchListener;
import com.dds.parade.Others.ModalCheckLink;
import com.dds.parade.Others.ModalComment;
import com.dds.parade.R;


public class PostDetailsFragment extends Fragment {
    ImageView imageView, btnBack, btnComment, btnAddToClip, btnFavorite;
    VideoView videoView;
    String type;
    int id;
    Button btnCheckLink;
    RelativeLayout llAddToClipList, rlCardContainer;
    TextView tvBackToCards;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_post_details, container, false);
        type = getArguments().getString("type");
        id = getArguments().getInt("id");

        imageView = view.findViewById(R.id.view_image);
        videoView = view.findViewById(R.id.view_video);
        rlCardContainer = view.findViewById(R.id.layout_card_container);
        rlCardContainer.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            @Override
            public void onSwipeDown() {
                Intent intentHome = new Intent(getActivity(), HomeActivity.class);
                startActivity(intentHome);
                getActivity().finish();
            }
        });
        llAddToClipList = getActivity().findViewById(R.id.layout_add_to_clip);
        /*tvBackToCards = view.findViewById(R.id.tv_back_to_cards);
        tvBackToCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animSlideRight = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);

                if (llAddToClipList.getVisibility() == View.VISIBLE) {
                    llAddToClipList.setAnimation(animSlideRight);
                    llAddToClipList.setVisibility(View.GONE);
                }
            }
        });*/

        if (type.equals("image")) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageResource(R.drawable.japan1);
        }

        else if (type.equals("video")) {
            videoView.setVisibility(View.VISIBLE);
            String path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.parade1;
            videoView.setVideoURI(Uri.parse(path));
            videoView.setTag("video" + id);

            /*videoView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    Toast.makeText(getActivity(), "Video Focus Changed", Toast.LENGTH_SHORT).show();
                    if (hasFocus) videoView.start();
                    else videoView.start();
                }
            });*/

//            videoView.start();
        }

        btnBack = view.findViewById(R.id.img_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentHome = new Intent(getActivity(), HomeActivity.class);
                startActivity(intentHome);
                getActivity().finish();
            }
        });

        btnComment = view.findViewById(R.id.img_comment);
        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModalComment modalComment = new ModalComment(v.getContext());
            }
        });

        //this button should be inside recyclerview adapter
        btnAddToClip = view.findViewById(R.id.img_add_to_clip);
        btnAddToClip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Visible hidden layouts to display two pages
                Intent intentAddToClipList = new Intent(getActivity(), AddToClipListActivity.class);
                startActivityForResult(intentAddToClipList, 1);
                getActivity().overridePendingTransition(R.anim.slide_up, R.anim.no_change);
                /*
                Animation animSlideUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
                if (llAddToClipList != null) {

                    if (llAddToClipList.getVisibility() == View.GONE) {
                        llAddToClipList.setAnimation(animSlideUp);
                        llAddToClipList.setVisibility(View.VISIBLE);

                        //checking
                        if (llAddToClipList.getVisibility() == View.VISIBLE) {
                            Toast.makeText(getActivity(), "Layout is visible now!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }*/
            }
        });

        btnCheckLink = view.findViewById(R.id.btn_check_link);
        btnCheckLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModalCheckLink modalCheckLink = new ModalCheckLink(v.getContext());
            }
        });

        btnFavorite = view.findViewById(R.id.img_favorite);
        btnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnFavorite.getContentDescription().equals("Selected")) {
                    btnFavorite.setImageResource(R.drawable.favorite_white);
                    btnFavorite.setContentDescription("Unselected");
                }
                else {
                    btnFavorite.setImageResource(R.drawable.favorite_red);
                    btnFavorite.setContentDescription("Selected");
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (requestCode == Activity.RESULT_OK) {
                Toast.makeText(getActivity(), "Clip is added successfully", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (id == 1 || id == 4) {
                Log.e("ASIF", id + "Fragment visible");
                if (videoView != null) videoView.start();
            }
        }
        else {
            if (id == 1 || id == 4) {
                Log.e("ASIF", id + "Fragment not visible");
                if (videoView != null) videoView.stopPlayback();
            }
        }
    }
}
