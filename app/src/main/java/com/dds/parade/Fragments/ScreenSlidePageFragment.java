package com.dds.parade.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dds.parade.Activity.PostDetailsActivity;
import com.dds.parade.Models.CategoryPostsResponse;
import com.dds.parade.Models.CategoryPostsTimelineData;
import com.dds.parade.Others.ModalComment;
import com.dds.parade.Others.ModalReport;
import com.dds.parade.R;
import com.dds.parade.RetrofitHelper;
import com.dds.parade.RetrofitService;
import com.dds.parade.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ScreenSlidePageFragment extends Fragment {
    ImageView imgPost;
    public LinearLayout llUserInfo;

    List<CategoryPostsTimelineData> postList = new ArrayList<>();
    RecyclerView recyclerView;
    PostsAdapter postsAdapter;

    RetrofitService service;
    int categoryID;

    boolean isLoading, isLastPage;
    int totalPages;
    int currentPage;

    SessionManager session;
    String sessionID;
    LinearLayoutManager layoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(
                R.layout.fragment_screen_slide_page, container,false);

        session = new SessionManager(getActivity());
        isLoading = false;
        isLastPage = false;
        totalPages = 1;
        currentPage = 1;

        service = RetrofitHelper.getRetrofitService();
        categoryID = getArguments().getInt("categoryID");

        postsAdapter = new PostsAdapter(postList);
        recyclerView = view.findViewById(R.id.recyclerPosts);
        llUserInfo = getActivity().findViewById(R.id.user_page);

        showPostsData();

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(postsAdapter);

        /********************************* Pagination *********************************/
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                Log.e("Asif-Pagination", "isLastPage: " + isLastPage);
                if (!isLastPage) {
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int firstVisibleItemCount = layoutManager.findFirstVisibleItemPosition();
                    int lastVisibleItem = layoutManager.findLastVisibleItemPosition();

                    if (!isLoading) {
//                    if ( (visibleItemCount + firstVisibleItemCount) >= totalItemCount && firstVisibleItemCount >= 0 && totalItemCount > 10 ) {
                        if (totalItemCount <= (lastVisibleItem + 1)) {
                            //End has been reached!
                            Log.e("Asif-Pagination", "End of Items has been reached!");
                            loadMoreItems();
                        }
                    }
                }
            }
        });

        /******************************************************************************/


        return view;
    }

    private void loadMoreItems() {
        Log.e("Asif-Pagination", "inside loadMoreItems()");
        isLoading = true;
        currentPage += 1;

        //Load next page items via page number
        Call<CategoryPostsResponse> categoryPostsCall = service.getPostsOfACategory(categoryID,"0vvqqc47gyd0c1ediobu1iuxk596d6qd", currentPage);
//        calls.add(findVideosCall);
        categoryPostsCall.enqueue(new Callback<CategoryPostsResponse>() {
            @Override
            public void onResponse(Call<CategoryPostsResponse> call, Response<CategoryPostsResponse> response) {
                isLoading = false;

                if (response.code() == 200) {
                    CategoryPostsResponse categoryPosts = response.body();
                    List<CategoryPostsTimelineData> temp = categoryPosts.getData().getTimelineData();
                    postList.addAll(temp);

                    Log.e("AsifNew", "" + postList.size());
                    postsAdapter.notifyDataSetChanged();

                    if (currentPage == totalPages) isLastPage = true;
                    else loadMoreItems();
                }
            }

            @Override
            public void onFailure(Call<CategoryPostsResponse> call, Throwable t) {

            }
        });
    }

    //// view Posts data after downloading from internet
    private void showPostsData () {
        Call<CategoryPostsResponse> categoryPostsCall = service.getPostsOfACategory(categoryID,"0vvqqc47gyd0c1ediobu1iuxk596d6qd", 1);
        categoryPostsCall.enqueue(new Callback<CategoryPostsResponse>() {
            @Override
            public void onResponse(Call<CategoryPostsResponse> call, Response<CategoryPostsResponse> response) {
                if (response.code() == 200) {
                    CategoryPostsResponse categoryPosts = response.body();
                    List<CategoryPostsTimelineData> temp = categoryPosts.getData().getTimelineData();
                    postList.addAll(temp);

                    Log.e("AsifNew", "" + postList.size());
                    postsAdapter.notifyDataSetChanged();

                    int totalCount = categoryPosts.getData().getTotalCount();
                    if((totalCount%10) == 0) totalPages = totalCount/10;
                    else totalPages = (totalCount/10) + 1;

                    if (currentPage == totalPages) isLastPage = true;
                }
            }

            @Override
            public void onFailure(Call<CategoryPostsResponse> call, Throwable t) {

            }
        });
    }





    /******************************* Adapter for Recycler view *******************************/
    public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.PostViewHolder>{

        private List<CategoryPostsTimelineData> postList;
        private static final int ITEM = 0;
        private static final int LOADING = 1;
        private boolean isLoadingAdded = false;

        public PostsAdapter(List<CategoryPostsTimelineData> postList) {
            this.postList = postList;
        }

        public class PostViewHolder extends RecyclerView.ViewHolder {

            //declare views (TextView) here
            public ImageView imagePreview, imageUser, imageFavorite, imageComment, imgReport;
            public TextView tvPostTitle, tvFavoriteTotal, tvCommentTotal;

            public PostViewHolder(View view) {
                super(view);

                //do findViewById here
                imagePreview = view.findViewById(R.id.post_preview);
                imageUser = view.findViewById(R.id.post_image_user);
                imageFavorite = view.findViewById(R.id.img_favorite);
                tvPostTitle = view.findViewById(R.id.post_title);
                tvFavoriteTotal = view.findViewById(R.id.tv_favorite_total);
                imageComment = view.findViewById(R.id.img_comment);
                tvCommentTotal = view.findViewById(R.id.tv_comment_total);
                imgReport = view.findViewById(R.id.img_report);
            }
        }

        @NonNull
        @Override
        public PostsAdapter.PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_post_item, parent, false);
            return new PostsAdapter.PostViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull final PostsAdapter.PostViewHolder holder, final int position) {
            //bind with views like setText() or listener;
            if (postList.get(position) != null) {
                final CategoryPostsTimelineData postData = postList.get(position);

                Picasso.get()
                        .load(postData.getPostUserProfilePic())
                        .into(holder.imageUser);

                if (postData.getPostCardType().equals("image")) {
                    Picasso.get()
                            .load(postData.getPostCard())
                            .into(holder.imagePreview);
                }

                holder.tvPostTitle.setText(postData.getPostTitle());
                holder.tvCommentTotal.setText(postData.getTotalComment().toString());
                holder.tvFavoriteTotal.setText(postData.getTotalFavorite().toString());


                /************************ OnClickListeners ************************/
                holder.imageComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ModalComment modalComment = new ModalComment(v.getContext());
                    }
                });


                holder.imageFavorite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (holder.imageFavorite.getContentDescription().equals("Selected")) {
                            holder.imageFavorite.setImageResource(R.drawable.favorite_white);
                            holder.imageFavorite.setContentDescription("Unselected");
                        } else {
                            holder.imageFavorite.setImageResource(R.drawable.favorite_red);
                            holder.imageFavorite.setContentDescription("Selected");
                        }
                    }
                });

                holder.imagePreview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intentPostDetails = new Intent(v.getContext(), PostDetailsActivity.class);
                        v.getContext().startActivity(intentPostDetails);
                    }
                });

                holder.imageUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Animation animSlideLeft = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_left);
                        if (llUserInfo != null) {
                            if (llUserInfo.getVisibility() == View.GONE) {
                                llUserInfo.setAnimation(animSlideLeft);
                                llUserInfo.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                });

                holder.imgReport.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (session.isLoggedIn()) {
                            String reportTitle = "Report Post";
                            String reportText = "Are you sure to report this post?";
                            ModalReport modalReport = new ModalReport(v.getContext(), "post", postData.getPostId(), reportTitle, reportText);
                        }
                        else {
                            new AlertDialog.Builder(getActivity())
                                    .setMessage("You need to be logged in to report any post!")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).show();
                        }
                    }
                });
            }

        }

        @Override
        public int getItemCount() {
            return postList.size();
        }
    }

}
