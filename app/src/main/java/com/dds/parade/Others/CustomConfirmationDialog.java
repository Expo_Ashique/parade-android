package com.dds.parade.Others;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyboardShortcutGroup;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.dds.parade.Models.Report;
import com.dds.parade.R;
import com.dds.parade.RetrofitHelper;
import com.dds.parade.RetrofitService;
import com.dds.parade.SessionManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CustomConfirmationDialog extends Dialog implements View.OnClickListener{

    Button btnOk, btnCancel;
    String type;
    int postId;
    RetrofitService service;

    SessionManager session;
    String sessionID;
    Context context;

    public CustomConfirmationDialog(@NonNull Context context, String type, int postId) {
        super(context);
        this.context = context;
        this.type = type;
        this.postId = postId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_confirmation);

        session = new SessionManager(context);
        if (session.isLoggedIn()) sessionID = session.getSessionID();

        btnCancel = findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
        btnOk = findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_ok:
                dismiss();
                service = RetrofitHelper.getRetrofitService();
                Call<Report> reportCall = service.submitReport(postId, sessionID, 1, "I hate this style of feeding child", "Unsocial");
                reportCall.enqueue(new Callback<Report>() {
                    @Override
                    public void onResponse(Call<Report> call, Response<Report> response) {
                        if (response.code() == 200) {
                            Report report = response.body();
                            Toast.makeText(getContext(),report.getMessage() + ", Post ID: " + postId, Toast.LENGTH_SHORT).show();
                        }

                        else Log.e("Asif-Report", "" + response.code());
                    }

                    @Override
                    public void onFailure(Call<Report> call, Throwable t) {

                    }
                });
            case  R.id.btn_cancel:
                dismiss();
        }
    }

}
