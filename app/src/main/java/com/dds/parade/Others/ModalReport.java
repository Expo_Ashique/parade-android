package com.dds.parade.Others;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.dds.parade.R;

public class ModalReport implements View.OnClickListener{
    Dialog dialog;
    String type;
    int postId;

    TextView tvReportTitle, tvReportText, tvSubmit, tvCancel;

    public ModalReport (Context context, String type, int postId, String reportTitle, String reportText) {
        dialog = new Dialog(context, R.style.Theme_AppCompat_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.modal_report);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogBottom;

        this.type = type;
        this.postId = postId;

        tvReportTitle = dialog.findViewById(R.id.tv_report_title);
        tvReportTitle.setText(reportTitle);
        tvReportText = dialog.findViewById(R.id.tv_report_text);
        tvReportText.setText(reportText);

        tvSubmit = dialog.findViewById(R.id.tv_submit);
        tvSubmit.setOnClickListener(this);
        tvCancel = dialog.findViewById(R.id.tv_cancel);
        tvCancel.setOnClickListener(this);

        dialog.show();
    }
    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.tv_submit:
                dialog.dismiss();
                CustomConfirmationDialog confirmDialog = new CustomConfirmationDialog(v.getContext(), type, postId);
                confirmDialog.show();
            case R.id.tv_cancel:
                dialog.dismiss();
        }
    }
}
