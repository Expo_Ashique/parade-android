package com.dds.parade.Others;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.dds.parade.Adapter.CommentsAdapter;
import com.dds.parade.Models.Comment;
import com.dds.parade.R;

import java.util.ArrayList;
import java.util.List;

public class ModalComment implements View.OnClickListener {
    Dialog dialog;
    ImageView btnClose;

    List<Comment> commentList = new ArrayList<>();
    RecyclerView recyclerView;
    CommentsAdapter commentsAdapter;

    public ModalComment(Context context) {
        dialog = new Dialog(context, R.style.Theme_AppCompat_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.modal_comment);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogBottom;
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        /************** Show all comments in Recyclerview  ******************/
        recyclerView = dialog.findViewById(R.id.recycler_comments);

        commentsAdapter = new CommentsAdapter(commentList);
        recyclerView.setLayoutManager(new LinearLayoutManager(dialog.getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(commentsAdapter);

        showCommentsData();

        btnClose = dialog.findViewById(R.id.img_close);
        btnClose.setOnClickListener(this);
        dialog.show();
    }

    private void showCommentsData () {
        Comment comment1 = new Comment();
        commentList.add(comment1);

        Comment comment2 = new Comment();
        commentList.add(comment2);

        Comment comment3 = new Comment();
        commentList.add(comment3);

        Comment comment4 = new Comment();
        commentList.add(comment4);

        Comment comment5 = new Comment();
        commentList.add(comment5);


        commentsAdapter.notifyDataSetChanged();
    }



    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.img_close:
                dialog.dismiss();
        }
    }
}
