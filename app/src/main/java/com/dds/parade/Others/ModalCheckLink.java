package com.dds.parade.Others;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.dds.parade.R;

public class ModalCheckLink implements View.OnClickListener{
    Dialog dialog;
    ImageView btnBack;
    WebView webViewLink;

    public ModalCheckLink(Context context) {
        dialog = new Dialog(context, R.style.Theme_AppCompat_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.modal_check_link);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogBottom;



        btnBack = dialog.findViewById(R.id.img_close_link);
        btnBack.setOnClickListener(this);
        dialog.show();

        webViewLink = dialog.findViewById(R.id.webview_link);
        webViewLink.getSettings().setJavaScriptEnabled(true);
        webViewLink.loadUrl("https://www.google.com/");
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.img_close_link:
                dialog.dismiss();
        }
    }
}
