package com.dds.parade.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dds.parade.Models.Post;
import com.dds.parade.R;

import java.util.List;

public class UserPostsAdapter extends RecyclerView.Adapter<UserPostsAdapter.UserPostViewHolder>{

    List<Post> postList;

    public UserPostsAdapter(List<Post> postList) {
        this.postList = postList;
    }

    public class UserPostViewHolder extends RecyclerView.ViewHolder {

        //declare views (TextView) here
        ImageView imgUser;

        public UserPostViewHolder(View view) {
            super(view);

            //do findViewById here
            imgUser = view.findViewById(R.id.post_image_user);
        }
    }


    @NonNull
    @Override
    public UserPostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_post_item, parent, false);
        return new UserPostsAdapter.UserPostViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull UserPostViewHolder holder, int position) {
        holder.imgUser.setImageResource(R.drawable.user2);
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }
}
