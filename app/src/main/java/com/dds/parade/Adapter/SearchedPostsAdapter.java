package com.dds.parade.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dds.parade.Models.Post;
import com.dds.parade.R;

import java.util.List;

public class SearchedPostsAdapter extends RecyclerView.Adapter<SearchedPostsAdapter.SearchedPostViewHolder>{

    List<Post> postList;

    public SearchedPostsAdapter(List<Post> postList) {
        this.postList = postList;
    }

    public class SearchedPostViewHolder extends RecyclerView.ViewHolder {

        //declare views (TextView) here

        public SearchedPostViewHolder(View itemView) {
            super(itemView);

            //do findViewById here
        }
    }


    @NonNull
    @Override
    public SearchedPostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_post_item, parent, false);
        return new SearchedPostViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchedPostViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return postList.size();
    }
}
