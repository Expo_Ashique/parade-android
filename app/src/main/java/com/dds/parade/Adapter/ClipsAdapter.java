package com.dds.parade.Adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dds.parade.Activity.ClipDetailsActivity;
import com.dds.parade.Models.Clip;
import com.dds.parade.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ClipsAdapter extends RecyclerView.Adapter<ClipsAdapter.clipsViewHolder> {

    private List<Clip> clipList;

    public ClipsAdapter(List<Clip> clipList) {
        this.clipList = clipList;
    }


    public class clipsViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTitle, tvItemTotal;
        public RelativeLayout rlClipItem;
        public ImageView imgPreview1, imgPreview2, imgPreview3, imgPreview4;

        public clipsViewHolder(View view) {
            super(view);
            tvTitle = view.findViewById(R.id.leftTitle);
            tvItemTotal = view.findViewById(R.id.rightNumber);
            rlClipItem = view.findViewById(R.id.layout_cliplist_item);
            imgPreview1 = view.findViewById(R.id.img_preview_1);
            imgPreview2 = view.findViewById(R.id.img_preview_2);
            imgPreview3 = view.findViewById(R.id.img_preview_3);
            imgPreview4 = view.findViewById(R.id.img_preview_4);
        }
    }

    @NonNull
    @Override
    public clipsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_clip_item, parent, false);
        return new ClipsAdapter.clipsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull clipsViewHolder holder, int position) {
        Clip clip = clipList.get(position);
        holder.tvTitle.setText(clip.getTitle());
        holder.tvItemTotal.setText(clip.getTotalItems());
        holder.rlClipItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentClipDetails = new Intent(v.getContext(), ClipDetailsActivity.class);
                v.getContext().startActivity(intentClipDetails);
            }
        });


        Picasso.get().load(R.drawable.japan1).into(holder.imgPreview1);
        Picasso.get().load(R.drawable.img_preview_cliplist).into(holder.imgPreview2);
        Picasso.get().load(R.drawable.img_preview_cliplist).into(holder.imgPreview3);
        Picasso.get().load(R.drawable.img_preview_cliplist).into(holder.imgPreview4);
    }

    @Override
    public int getItemCount() {
        return clipList.size();
    }


}
