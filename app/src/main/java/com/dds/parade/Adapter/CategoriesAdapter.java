package com.dds.parade.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dds.parade.R;

import java.util.List;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoryViewHolder>{
    List<String> categoryList;
    int selectedPosition = -1;
    LayoutInflater layoutInflater;

    public class CategoryViewHolder extends RecyclerView.ViewHolder{

        TextView tvCategory;
        LinearLayout llCategoryItem;

        public CategoryViewHolder(View view) {
            super(view);

            tvCategory = view.findViewById(R.id.tv_category);
            llCategoryItem = view.findViewById(R.id.layout_category_item);
        }

    }


    public CategoriesAdapter(Context context, List<String> categoryList) {
        this.layoutInflater = LayoutInflater.from(context);
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_category_item, parent, false);
        return new CategoriesAdapter.CategoryViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, final int position) {
        String categoryItem = categoryList.get(position);
        holder.tvCategory.setText(categoryItem);

        if (selectedPosition == position) holder.llCategoryItem.setBackgroundResource(R.drawable.highlight);
        else holder.llCategoryItem.setBackgroundResource(R.drawable.white_background);

        holder.llCategoryItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }
}
