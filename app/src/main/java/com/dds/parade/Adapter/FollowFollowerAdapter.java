package com.dds.parade.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dds.parade.Models.Follow;
import com.dds.parade.R;

import java.util.List;

public class FollowFollowerAdapter extends RecyclerView.Adapter<FollowFollowerAdapter.FollowFollowerViewHolder>{

    List<Follow> followsList;

    public FollowFollowerAdapter(List<Follow> followsList) {
        this.followsList = followsList;
    }

    public class FollowFollowerViewHolder extends RecyclerView.ViewHolder {

        public FollowFollowerViewHolder(View view) {
            super(view);
        }
    }


    @NonNull
    @Override
    public FollowFollowerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_follow_follower_bar_item, parent, false);
        return new FollowFollowerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FollowFollowerViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return followsList.size();
    }


}
