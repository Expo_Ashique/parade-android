package com.dds.parade.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dds.parade.Models.Notification;
import com.dds.parade.R;

import java.util.List;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.notificationViewHolder>{

    private List<Notification> notificationsList;

    public NotificationsAdapter(List<Notification> notificationsList) {
        this.notificationsList = notificationsList;
    }



    public class notificationViewHolder extends RecyclerView.ViewHolder {

        public ImageView imgUser;
        public TextView tvTitle, tvDescription;

        public notificationViewHolder(View view) {
            super(view);
            imgUser = view.findViewById(R.id.imageView);
            tvTitle = view.findViewById(R.id.notficationTitle);
            tvDescription = view.findViewById(R.id.notficationDescription);
        }
    }

    @NonNull
    @Override
    public notificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notification_item, parent, false);
        return new notificationViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull notificationViewHolder holder, int position) {
        Notification notification = notificationsList.get(position);

//        holder.imgUser.setText(notification.getUserImgURL());
        holder.tvTitle.setText(notification.getTitle());
        holder.tvDescription.setText(notification.getDescription());
    }

    @Override
    public int getItemCount() {
        return notificationsList.size();
    }


}
