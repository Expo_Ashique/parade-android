package com.dds.parade.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dds.parade.Models.Post;
import com.dds.parade.Others.ModalComment;
import com.dds.parade.R;

import java.util.List;

public class FavoritesAdapter extends RecyclerView.Adapter<FavoritesAdapter.FavoritesViewHolder>{

    List<Post> postList;

    public FavoritesAdapter(List<Post> postList) {
        this.postList = postList;
    }


    @NonNull
    @Override
    public FavoritesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_post_item, parent, false);
        return new FavoritesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final FavoritesViewHolder holder, final int position) {
        holder.btnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.btnFavorite.getContentDescription().equals("Selected")) {
                    holder.btnFavorite.setImageResource(R.drawable.favorite_white);
                    holder.btnFavorite.setContentDescription("Unselected");
                }
                else {
                    holder.btnFavorite.setImageResource(R.drawable.favorite_red);
                    holder.btnFavorite.setContentDescription("Selected");
                }
            }
        });

        holder.btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModalComment modalComment = new ModalComment(v.getContext());
            }
        });
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class FavoritesViewHolder extends RecyclerView.ViewHolder {

        public ImageView btnFavorite, btnComment;

        public FavoritesViewHolder(View v) {
            super(v);
            btnFavorite = v.findViewById(R.id.img_favorite);
            btnComment = v.findViewById(R.id.img_comment);
        }
    }
}
