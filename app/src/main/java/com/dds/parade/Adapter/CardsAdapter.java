package com.dds.parade.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.dds.parade.Activity.ClipItemViewActivity;
import com.dds.parade.Models.Card;
import com.dds.parade.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

public class CardsAdapter extends RecyclerView.Adapter<CardsAdapter.CardsViewHolder>{

    List<Card> cardList;
    Context context;
    Glide glide;

    public CardsAdapter (List<Card> cardList) {
        this.cardList = cardList;
    }

    public class CardsViewHolder extends RecyclerView.ViewHolder {

        public TextView tvCardTitle;
        public ImageView imgCard, imgVideoPlay;
        public LinearLayout llClipCard;

        public CardsViewHolder(View v) {
            super(v);
            context = v.getContext();
            tvCardTitle = v.findViewById(R.id.tv_card_title);
            imgCard = v.findViewById(R.id.img_card);
            imgVideoPlay = v.findViewById(R.id.img_video_play);
            llClipCard = v.findViewById(R.id.layout_clip_card);
        }
    }

    @NonNull
    @Override
    public CardsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_clip_details_item, parent,false);
        return new CardsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final CardsViewHolder holder, final int position) {
        Card card = cardList.get(position);

        final String type = card.getType();
        final String src = card.getSrc();

        if (type.equals("image")) {
            if (src.equals("japan1")) {
                holder.imgCard.setContentDescription("japan1");
                Picasso.get().load(R.drawable.japan1).into(holder.imgCard);
//                holder.imgCard.setImageResource(R.drawable.japan1);
            }
            else {
                holder.imgCard.setContentDescription("japan2");
                Picasso.get().load(R.drawable.japan2).into(holder.imgCard);
//                holder.imgCard.setImageResource(R.drawable.japan2);
            }
        }

        else {
            holder.imgVideoPlay.setVisibility(View.VISIBLE);
            String vidPath = Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator + "Parade" + File.separator + "parade1.mp4";
            glide.with(context)
                    .load(vidPath)
                    .crossFade()
                    .into(holder.imgCard);
        }

        holder.llClipCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentClipItemView = new Intent(v.getContext(), ClipItemViewActivity.class);
                intentClipItemView.putExtra("type", type);
                Log.e("Asif", type);
                if (type.equals("image")) intentClipItemView.putExtra("src", holder.imgCard.getContentDescription());
                else intentClipItemView.putExtra("src", "parade1.mp4");
                v.getContext().startActivity(intentClipItemView);
            }
        });

        holder.tvCardTitle.setText(card.getTitle());
    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }

}
