package com.dds.parade.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;
import android.widget.VideoView;

import com.dds.parade.Fragments.PostDetailsFragment;
import com.dds.parade.R;

public class PostDetailsActivity extends AppCompatActivity {

    ViewPager viewPager;
    PostDetailsPagerAdapter postDetailsPagerAdapter;

    LinearLayout llItemIndicator;
    int indicatorWidth;
    int leftMargin;
    int rightMargin;
    final int TOTAL_ITEMS = 5;
    int prevPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_post_details);

        /* Get the screen width */
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        indicatorWidth = displayMetrics.widthPixels / TOTAL_ITEMS;
        leftMargin = rightMargin = displayMetrics.widthPixels/100;
//        Toast.makeText(this, "Indicator Width: " + indicatorWidth, Toast.LENGTH_SHORT).show();


        /**************** Work with Indicator at Top here ****************/
        llItemIndicator = findViewById(R.id.item_indicator);

        for (int i=0;i<TOTAL_ITEMS;i++) {
            View indicator = new View(this);
            LayoutParams params = new LayoutParams(indicatorWidth - (leftMargin + rightMargin), ViewGroup.LayoutParams.MATCH_PARENT);
            params.setMargins(leftMargin,0,leftMargin,0);
            indicator.setLayoutParams(params);
            indicator.setTag("indicator" + i);
            indicator.setBackgroundResource(R.drawable.highlight_indicator);
            indicator.setEnabled(false);
            llItemIndicator.addView(indicator);
        }

        //Always enable 1st indicator
        View indicator1 = llItemIndicator.findViewWithTag("indicator" + 0);
        indicator1.setEnabled(true);




        /**************** Work with View Pager here ****************/
        viewPager = findViewById(R.id.pager_post_details);
        prevPage = 0;
        postDetailsPagerAdapter = new PostDetailsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(postDetailsPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                Toast.makeText(PostDetailsActivity.this, "Page Selected: " + position, Toast.LENGTH_SHORT).show();
                for (int i=1;i<=position;i++) {
                    View indicator = llItemIndicator.findViewWithTag("indicator" + i);
                    indicator.setEnabled(true);
                }
                for (int j = position + 1; j < TOTAL_ITEMS; j++) {
                    View indicator = llItemIndicator.findViewWithTag("indicator" + j);
                    indicator.setEnabled(false);
                }

                //check if previous page has any Video of it and stop it's playback

                //if this page has video, start it
                /*PostDetailsFragment frag = (PostDetailsFragment) viewPager.getAdapter().instantiateItem(viewPager, position);
                if (frag != null) {
                    VideoView videoView = frag.getView().findViewWithTag("video" + position);
                    if (videoView != null) {
                        videoView.start();
                    }
                }*/

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    /**
     * A simple pager adapter that represents 5 PostDetailsFragment objects, in
     * sequence.
     */

    private class PostDetailsPagerAdapter extends FragmentStatePagerAdapter {

        public PostDetailsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();

            if (position == 0) bundle.putString("type", "image");
            else if (position == 1) {
                bundle.putString("type", "video");
                bundle.putInt("id", 1);
            }
            else if (position == 2) bundle.putString("type", "image");
            else if (position == 3) bundle.putString("type", "image");
            else if (position == 4) {
                bundle.putString("type", "video");
                bundle.putInt("id", 4);
            }

            PostDetailsFragment postDetailsFragment = new PostDetailsFragment();
            postDetailsFragment.setArguments(bundle);
            return postDetailsFragment;
        }

        @Override
        public int getCount() {
            return TOTAL_ITEMS;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intentHome = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intentHome);
    }
}
