package com.dds.parade.Activity;

import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.dds.parade.OnSwipeTouchListener;
import com.dds.parade.R;
import com.dds.parade.SimpleGestureFilter;

import java.io.File;

public class ClipItemViewActivity extends AppCompatActivity implements View.OnClickListener{

    ImageView imgCard;
    VideoView videoCard;
    ImageButton btnClose;

    RelativeLayout clipItem;

    String type, src;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_clip_item_view);

//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int width = displayMetrics.widthPixels;
//        int height = displayMetrics.heightPixels;

        clipItem = findViewById(R.id.layout_clip_item);
        clipItem.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeDown() {
                finish();
            }
        });

        type = getIntent().getStringExtra("type");
        src = getIntent().getStringExtra("src");

        imgCard = findViewById(R.id.img_clip_card);
        videoCard = findViewById(R.id.video_clip_card);

        if (type.equals("video")) {
            imgCard.setVisibility(View.GONE);
            videoCard.setVisibility(View.VISIBLE);
//            videoCard.setLayoutParams(new FrameLayout.LayoutParams(width, height));
            String path = "android.resource://" + getPackageName() + "/" + R.raw.parade1;
            videoCard.setVideoURI(Uri.parse(path));
            videoCard.start();
        }

        else if (type.equals("image")) {
            imgCard.setVisibility(View.VISIBLE);
            videoCard.setVisibility(View.GONE);
            if (src.equals("japan1")) imgCard.setImageResource(R.drawable.japan1);
            else imgCard.setImageResource(R.drawable.japan2);
        }

        btnClose = findViewById(R.id.btn_close_clip_card);
        btnClose.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_close_clip_card:
                finish();
                break;
        }
    }
}
