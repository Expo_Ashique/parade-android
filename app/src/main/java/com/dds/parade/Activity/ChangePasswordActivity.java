package com.dds.parade.Activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.dds.parade.R;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener{

    TextView tvBack;
    Button btnSave;

    AutoCompleteTextView etCurrentPass, etNewPass, etConfirmPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        tvBack = findViewById(R.id.tv_back_to_settings_page);
        tvBack.setOnClickListener(this);

        btnSave = findViewById(R.id.btn_save_password);
        btnSave.setOnClickListener(this);

        etCurrentPass = findViewById(R.id.et_current_password);
        etNewPass = findViewById(R.id.et_new_password);
        etConfirmPass = findViewById(R.id.et_confirm_password);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.tv_back_to_settings_page:
                finish();
                break;
            case R.id.btn_save_password:
                String currentPass = etCurrentPass.getText().toString().trim();
                String newPass = etNewPass.getText().toString().trim();
                String confirmPass = etConfirmPass.getText().toString().trim();

                if (currentPass.equals("") || newPass.equals("") || confirmPass.equals("")) {
                    new AlertDialog.Builder(this)
                            .setMessage("Please fill up all fields properly")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            }).show();
                }
                else {
                    finish();
                }
                break;
        }
    }
}
