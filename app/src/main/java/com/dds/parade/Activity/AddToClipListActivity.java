package com.dds.parade.Activity;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dds.parade.Adapter.ClipsAdapter;
import com.dds.parade.Models.Clip;
import com.dds.parade.R;

import java.util.ArrayList;
import java.util.List;

public class AddToClipListActivity extends AppCompatActivity {
    TextView tvBackToCards, tvBackToClipList;
    List<Clip> clipList = new ArrayList<>();
    RecyclerView recyclerView;
    NewClipsAdapter clipsAdapter;
    AutoCompleteTextView tvClipName;

    Button btnCreateNewClip, btnAddNewClipName;

    LinearLayout llNewClipName;
    RelativeLayout rlMainContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_clip_list);

        tvClipName = findViewById(R.id.tv_new_name);

        recyclerView = findViewById(R.id.recycler_clips_add);
        recyclerView.setNestedScrollingEnabled(false);

        clipsAdapter = new NewClipsAdapter(clipList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(clipsAdapter);

        showClipsData();


        tvBackToCards = findViewById(R.id.tv_back_to_cards);
        tvBackToCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentReturn = new Intent();
                setResult(Activity.RESULT_OK, intentReturn);
                finish();
                overridePendingTransition(R.anim.no_change, R.anim.slide_down);
            }
        });

        tvBackToClipList = findViewById(R.id.tv_back_to_clip_list);
        tvBackToClipList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(tvClipName.getWindowToken(), 0);

                Animation animSlideRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);

                if (llNewClipName.getVisibility() == View.VISIBLE) {
                    llNewClipName.setAnimation(animSlideRight);
                    llNewClipName.setVisibility(View.GONE);
                    rlMainContent.setVisibility(View.VISIBLE);
                }
            }
        });

        rlMainContent = findViewById(R.id.layout_add_clip_list_main);
        llNewClipName = findViewById(R.id.layout_new_clip_name);

        btnAddNewClipName = findViewById(R.id.btn_add_new_clip);
        btnAddNewClipName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(tvClipName.getWindowToken(), 0);

                Animation animSlideRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);

                if (llNewClipName.getVisibility() == View.VISIBLE) {
                    llNewClipName.setAnimation(animSlideRight);
                    llNewClipName.setVisibility(View.GONE);
                    rlMainContent.setVisibility(View.VISIBLE);
                }

                Clip clip = new Clip(tvClipName.getText().toString(), "12", "");
                clipList.add(clip);

                clipsAdapter.notifyDataSetChanged();
            }
        });

        btnCreateNewClip = findViewById(R.id.btn_create_new_clip);
        btnCreateNewClip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animSlideRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);

                if (llNewClipName.getVisibility() == View.GONE) {
                    llNewClipName.setAnimation(animSlideRight);
                    llNewClipName.setVisibility(View.VISIBLE);
                    rlMainContent.setVisibility(View.GONE);
                }
            }
        });


    }

    private void showClipsData () {
        Clip clip1 = new Clip("California Trip", "12", "");
        clipList.add(clip1);

        Clip clip2 = new Clip("California Trip", "12", "");
        clipList.add(clip2);

        Clip clip3 = new Clip("California Trip", "12", "");
        clipList.add(clip3);

        Clip clip4 = new Clip("California Trip", "12", "");
        clipList.add(clip4);

        Clip clip5 = new Clip("California Trip", "12", "");
        clipList.add(clip5);

        Clip clip6 = new Clip("California Trip", "12", "");
        clipList.add(clip6);

        Clip clip7 = new Clip("California Trip", "12", "");
        clipList.add(clip7);


        clipsAdapter.notifyDataSetChanged();
    }

    public class NewClipsAdapter extends RecyclerView.Adapter<NewClipsAdapter.NewClipsViewHolder> {

        private List<Clip> clipList;

        public NewClipsAdapter(List<Clip> clipList) {
            this.clipList = clipList;
        }


        public class NewClipsViewHolder extends RecyclerView.ViewHolder {

            public TextView tvTitle, tvItemTotal;
            public RelativeLayout rlRecyclerItem;

            public NewClipsViewHolder(View view) {
                super(view);
                tvTitle = view.findViewById(R.id.leftTitle);
                tvItemTotal = view.findViewById(R.id.rightNumber);
                rlRecyclerItem = view.findViewById(R.id.layout_cliplist_item);
            }
        }

        @NonNull
        @Override
        public NewClipsAdapter.NewClipsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_clip_item, parent, false);
            return new NewClipsAdapter.NewClipsViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull NewClipsAdapter.NewClipsViewHolder holder, int position) {
            Clip clip = clipList.get(position);
            holder.tvTitle.setText(clip.getTitle());
            holder.tvItemTotal.setText(clip.getTotalItems());
            holder.rlRecyclerItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(AddToClipListActivity.this, "Clip Added Successfully", Toast.LENGTH_SHORT).show();
                    Intent intentReturn = new Intent();
                    setResult(Activity.RESULT_OK, intentReturn);
                    finish();
                    overridePendingTransition(R.anim.no_change, R.anim.slide_down);
                }
            });
        }

        @Override
        public int getItemCount() {
            return clipList.size();
        }


    }

    @Override
    public void onBackPressed() {
        // Do nothing
    }
}
