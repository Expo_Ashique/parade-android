package com.dds.parade.Activity;

import android.content.Intent;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.dds.parade.Adapter.FollowFollowerAdapter;
import com.dds.parade.Adapter.UserPostsAdapter;
import com.dds.parade.FullDrawerLayout;
import com.dds.parade.Models.Follow;
import com.dds.parade.Models.Post;
import com.dds.parade.R;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;
import java.util.List;

public class UserPageActivity extends AppCompatActivity {

    List<Post> userPostList = new ArrayList<>();
    RecyclerView recyclerUserPosts;
    UserPostsAdapter userPostsAdapter;

    ScrollView scrollView;
    TextView tvTitle;
    TextView tvHiddenTitle;
    View hiddenDivider;

    ScrollView scrollViewFollowFollower;
    TextView tvTitleFollowFollower;
    TextView tvHiddenTitleFollowFollower;
    View hiddenDividerFollowFollower;

    String drawerTitle;

    List<Follow> followsList = new ArrayList<>();
    RecyclerView recyclerFollowFollower;
    FollowFollowerAdapter followFollowerAdapter;

    TextView tvFollow;
    TextView tvFollower;

    ImageView imgBack;

    FullDrawerLayout drawerLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_page);

        /************** Show all Posts of User in Recycler View  ******************/
        userPostsAdapter = new UserPostsAdapter(userPostList);

        recyclerUserPosts = findViewById(R.id.recyclerUserPosts);
        recyclerUserPosts.setNestedScrollingEnabled(false);
        recyclerUserPosts.setLayoutManager(new LinearLayoutManager(this));
        recyclerUserPosts.setItemAnimator(new DefaultItemAnimator());
        recyclerUserPosts.setAdapter(userPostsAdapter);
        recyclerUserPosts.setFocusable(false);

        showMyPostsData();

        /************** Show all Follow/Followers in Recycler View  ******************/
        followFollowerAdapter = new FollowFollowerAdapter(followsList);

        recyclerFollowFollower = findViewById(R.id.recycler_follow_follower_likes);
        recyclerFollowFollower.setNestedScrollingEnabled(false);
        recyclerFollowFollower.setLayoutManager(new LinearLayoutManager(this));
        recyclerFollowFollower.setItemAnimator(new DefaultItemAnimator());
        recyclerFollowFollower.setAdapter(followFollowerAdapter);
        recyclerFollowFollower.setFocusable(false);


//        tvTitle = findViewById(R.id.title_user_page);
//        tvHiddenTitle = findViewById(R.id.title_hidden);
//        hiddenDivider = findViewById(R.id.hidden_divider);

        tvTitleFollowFollower = findViewById(R.id.title_follow);
        tvHiddenTitleFollowFollower = findViewById(R.id.title_hidden_follow);
        hiddenDividerFollowFollower = findViewById(R.id.hidden_divider_follow);



        drawerLayout = findViewById(R.id.drawer_user_page);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                hiddenDividerFollowFollower.setVisibility(View.INVISIBLE);
                tvHiddenTitleFollowFollower.setVisibility(View.INVISIBLE);

                if (drawerTitle != null) {
                    tvTitleFollowFollower.setText(drawerTitle);
                    tvHiddenTitleFollowFollower.setText(drawerTitle);
                }
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {


            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                followsList.clear();
                followFollowerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        tvFollow = findViewById(R.id.tv_follow_total);
        tvFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerTitle = "FOLLOW";
//                tvTitleFollowFollower.setText("FOLLOW");
//                tvHiddenTitleFollowFollower.setText("FOLLOW");
//                tvHiddenTitleFollowFollower.setVisibility(View.INVISIBLE);
                showFollowsData();
                drawerLayout.openDrawer(Gravity.END);
            }
        });
        tvFollower = findViewById(R.id.tv_follower_total);
        tvFollower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerTitle = "FOLLOWER";
//                tvTitleFollowFollower.setText("FOLLOWER");
//                tvHiddenTitleFollowFollower.setText("FOLLOWER");
                showFollowsData();
                drawerLayout.openDrawer(Gravity.END);
            }
        });

        imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(Gravity.END);

            }
        });


        /******************** Animation while scrolling all Posts *******************************/
        /*scrollView = findViewById(R.id.scroll_user_page_content);
//        scrollView.smoothScrollTo(0, 0);
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {

                Rect scrollBounds = new Rect();
                scrollView.getHitRect(scrollBounds);
                if (tvTitle.getLocalVisibleRect(scrollBounds)) {
                    // if layout even a single pixel, is within the visible area do something
                    tvHiddenTitle.setVisibility(View.INVISIBLE);
                    hiddenDivider.setVisibility(View.INVISIBLE);

                } else {
                    // if layout goes out or scrolled out of the visible area do something
//                    Toast.makeText(MyPageActivity.this, "Title is out of focus!", Toast.LENGTH_SHORT).show();
                    tvHiddenTitle.setVisibility(View.VISIBLE);
                    hiddenDivider.setVisibility(View.VISIBLE);
                }

            }
        }); */


        /******************** Animation while scrolling all Follow/Followers *******************************/
        scrollViewFollowFollower = findViewById(R.id.scroll_follow_follower);
//        scrollView.smoothScrollTo(0, 0);
        scrollViewFollowFollower.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {

                Rect scrollBounds = new Rect();
                scrollViewFollowFollower.getHitRect(scrollBounds);
                if (tvTitleFollowFollower.getLocalVisibleRect(scrollBounds)) {
                    // if layout even a single pixel, is within the visible area do something
                    tvHiddenTitleFollowFollower.setVisibility(View.INVISIBLE);
                    hiddenDividerFollowFollower.setVisibility(View.INVISIBLE);

                } else {
                    // if layout goes out or scrolled out of the visible area do something
//                    Toast.makeText(MyPageActivity.this, "Title is out of focus!", Toast.LENGTH_SHORT).show();
                    tvHiddenTitleFollowFollower.setVisibility(View.VISIBLE);
                    hiddenDividerFollowFollower.setVisibility(View.VISIBLE);
                }

            }
        });

        /********************** Bottom Navigation Bar ********************/

        BottomNavigationViewEx bottomNavigationViewEx = findViewById(R.id.bottom_navigation);
        bottomNavigationViewEx.enableAnimation(false);
        bottomNavigationViewEx.enableItemShiftingMode(false);
        bottomNavigationViewEx.enableShiftingMode(false);
        bottomNavigationViewEx.setSelectedItemId(R.id.action_myPage);
        bottomNavigationViewEx.setIconSize(25f, 25f);
        bottomNavigationViewEx.setTextVisibility(false);

        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_home:
                        Intent intentHome = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(intentHome);
                        break;
                    case R.id.action_clipList:
                        Intent intentClipList = new Intent(getApplicationContext(), ClipListActivity.class);
                        startActivity(intentClipList);
                        break;
                    case R.id.action_search:
                        Intent intentSearch = new Intent(getApplicationContext(), SearchActivity.class);
                        startActivity(intentSearch);
                        break;
                    case R.id.action_notification:
                        Intent intentNotify = new Intent(getApplicationContext(), NotifyActivity.class);
                        startActivity(intentNotify);
                        break;
                    case R.id.action_myPage:
                        break;
                }
                return true;
            }
        });
    }

    private void showMyPostsData () {
        Post post1 = new Post();
        userPostList.add(post1);

        Post post2 = new Post();
        userPostList.add(post2);

        Post post3 = new Post();
        userPostList.add(post3);

        Post post4 = new Post();
        userPostList.add(post4);

        Post post5 = new Post();
        userPostList.add(post5);

        Post post6 = new Post();
        userPostList.add(post6);

        userPostsAdapter.notifyDataSetChanged();
    }

    private void showFollowsData () {
        Follow follow1 = new Follow();
        followsList.add(follow1);

        Follow follow2 = new Follow();
        followsList.add(follow2);

        Follow follow3 = new Follow();
        followsList.add(follow3);

        Follow follow4 = new Follow();
        followsList.add(follow4);

        Follow follow5 = new Follow();
        followsList.add(follow5);

        Follow follow6 = new Follow();
        followsList.add(follow6);

        Follow follow7 = new Follow();
        followsList.add(follow7);

        Follow follow8 = new Follow();
        followsList.add(follow8);

        Follow follow9 = new Follow();
        followsList.add(follow9);

        Follow follow10 = new Follow();
        followsList.add(follow10);

        Follow follow11 = new Follow();
        followsList.add(follow11);

        Follow follow12 = new Follow();
        followsList.add(follow12);

        Follow follow13 = new Follow();
        followsList.add(follow13);

        Follow follow14 = new Follow();
        followsList.add(follow14);

        Follow follow15 = new Follow();
        followsList.add(follow15);

        Follow follow16 = new Follow();
        followsList.add(follow16);

        Follow follow17 = new Follow();
        followsList.add(follow17);

        followFollowerAdapter.notifyDataSetChanged();
    }
}
