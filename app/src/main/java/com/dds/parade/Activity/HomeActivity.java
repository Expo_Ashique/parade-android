package com.dds.parade.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.design.widget.BottomNavigationView;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.dds.parade.Adapter.CategoriesAdapter;
import com.dds.parade.Adapter.FollowFollowerAdapter;
import com.dds.parade.Adapter.UserPostsAdapter;
import com.dds.parade.Models.CategoryResult;
import com.dds.parade.NetworkConnectionHelper;
import com.dds.parade.RetrofitHelper;
import com.dds.parade.RetrofitService;
import com.dds.parade.FullDrawerLayout;
import com.dds.parade.Listeners.RecyclerTouchListener;
import com.dds.parade.Models.Category;
import com.dds.parade.Models.Follow;
import com.dds.parade.Models.Post;
import com.dds.parade.Others.ModalReport;
import com.dds.parade.R;
import com.dds.parade.Fragments.ScreenSlidePageFragment;
import com.dds.parade.SessionManager;
import com.dds.parade.SimpleGestureFilter;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends FragmentActivity{
//    List<Post> postList = new ArrayList<>();
//    RecyclerView recyclerViewPosts;
//    PostsAdapter postsAdapter;

    List<String> categoryList;
    RecyclerView recyclerViewCategories;
    CategoriesAdapter categoriesAdapter;

    private static int NUM_PAGES;
    private ViewPager mPager;
    private ScreenSlidePagerAdapter mPagerAdapter;

    private SimpleGestureFilter detector;
    LinearLayout llPosts;
    TextView tvCategory;
    HorizontalScrollView hsv;
    int offset;

    SessionManager session;

//    String[] categories = {"おでかけ","コスメ","レディース","メンズ","インテリア","恋愛","テクノロジー","エンタメ","スポーツ","健康","アート"};

    ImageView imgBackToTimeline;
    LinearLayout llUserInfo;

    /******** Variables for User Page **************/
    List<Post> userPostList = new ArrayList<>();
    RecyclerView recyclerUserPosts;
    UserPostsAdapter userPostsAdapter;

    ScrollView scrollViewFollowFollower;
    TextView tvTitleFollowFollower;
    TextView tvHiddenTitleFollowFollower;
    View hiddenDividerFollowFollower;

    String drawerTitle;

    List<Follow> followsList = new ArrayList<>();
    RecyclerView recyclerFollowFollower;
    FollowFollowerAdapter followFollowerAdapter;

    TextView tvFollow, tvFollower;

    ImageView imgBack, imgReportUser;

    Button btnFollowUser;

    FullDrawerLayout drawerLayout;

    RetrofitService retrofitService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        session = new SessionManager(this);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int screenWidth = displayMetrics.widthPixels;
        offset = screenWidth/2;

        //First copy the video file to internal storage
        String vidPath = Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator + "Parade" + File.separator + "parade1.mp4";
        File file = new File(vidPath);

        if (!file.exists()) {
            try {
                String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Parade";
                File dir = new File(path);
                if (dir.mkdirs() || dir.isDirectory()) {
                    CopyRAWtoSDCard(R.raw.parade1, path + File.separator + "parade1.mp4");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        retrofitService = RetrofitHelper.getRetrofitService();

        mPager = (ViewPager) findViewById(R.id.pager);


        categoryList = new ArrayList<>();
        categoriesAdapter = new CategoriesAdapter(this, categoryList);

        /********************************************* Check if Network is connected **********************************************/
        if (NetworkConnectionHelper.isConnected(this)) {

            ////////////// Get all Category List
            Call<Category> categoryListCall = retrofitService.getAllCategories();
            categoryListCall.enqueue(new Callback<Category>() {
                @Override
                public void onResponse(Call<Category> call, Response<Category> response) {
                    if (response.code() == 200) {
                        Log.e("Asif", "Categories found!");
                        Category category = response.body();
                        List<CategoryResult> categoryResults = category.getData().getResult();

                        for (CategoryResult categoryResult : categoryResults) {
                            String catTitle = categoryResult.getCategoryTitle();
                            categoryList.add(catTitle);
                        }
                        NUM_PAGES = categoryList.size();
                        categoriesAdapter.notifyDataSetChanged();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                recyclerViewCategories.findViewHolderForAdapterPosition(0).itemView.performClick();
                            }
                        },500);

                        /////// now create all pages dynamically in View Pager
                        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
                        mPager.setAdapter(mPagerAdapter);

                    } else Log.e("Asif","" + response.code());
                }

                @Override
                public void onFailure(Call<Category> call, Throwable t) {
                    Log.e("Asif", "Failed to connect!");
                }
            });


            recyclerViewCategories = findViewById(R.id.recyclerCategory);
            LinearSnapHelper snapHelper = new LinearSnapHelper();
            snapHelper.attachToRecyclerView(recyclerViewCategories);
            recyclerViewCategories.setOnFlingListener(snapHelper);
            recyclerViewCategories.setItemAnimator(new DefaultItemAnimator());
            recyclerViewCategories.setAdapter(categoriesAdapter);
//        offset = recyclerViewCategories.computeHorizontalScrollOffset();

            recyclerViewCategories.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerViewCategories, new RecyclerTouchListener.ClickListener() {
                //If user click any category in Top Bar
                @Override
                public void onClick(View view, int position) {
                    int targetPosition = 0;
                    String itemClicked = categoryList.get(position);
//                Toast.makeText(HomeActivity.this, itemClicked, Toast.LENGTH_SHORT).show();
                    mPager.setCurrentItem(position, true);

                    //find middle position and focus item there in the recyclerview
                    LinearLayoutManager llmanger = (LinearLayoutManager) recyclerViewCategories.getLayoutManager();

                    int firstVisibleItemPosition = llmanger.findFirstVisibleItemPosition();
                    int lastVisibleItemPosition = llmanger.findLastVisibleItemPosition();

                    int totalItems = lastVisibleItemPosition - firstVisibleItemPosition;

                    if (position == firstVisibleItemPosition) {
                        targetPosition = position - totalItems/2;
                    }
                    if (position == firstVisibleItemPosition) {
                        targetPosition = position + totalItems/2;
                    }
                }
            }));

        }

        else
        {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("Not connected to the Internet")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }




        llUserInfo = findViewById(R.id.user_page);
        imgBackToTimeline = findViewById(R.id.img_back_to_timeline);
        imgBackToTimeline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animSlideRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_right);

                if (llUserInfo.getVisibility() == View.VISIBLE) {
                    llUserInfo.setAnimation(animSlideRight);
                    llUserInfo.setVisibility(View.GONE);
                }
            }
        });

        // Instantiate a ViewPager and a PagerAdapter.
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            // If user swipe through viewpager change the highlighted item in Top bar
            @Override
            public void onPageSelected(int position) {
                int targetPosition = 0;
//                Toast.makeText(HomeActivity.this, "Page changed to: " + position, Toast.LENGTH_SHORT).show();

                LinearLayoutManager llmanger = (LinearLayoutManager) recyclerViewCategories.getLayoutManager();
                /*int firstVisibleItemPosition = llmanger.findFirstVisibleItemPosition();
                int lastVisibleItemPosition = llmanger.findLastVisibleItemPosition();

                int totalItems = lastVisibleItemPosition - firstVisibleItemPosition;

                if (position == firstVisibleItemPosition) {
                    targetPosition = position - totalItems/2;
                }
                if (position == firstVisibleItemPosition) {
                    targetPosition = position + totalItems/2;
                }*/

                llmanger.scrollToPositionWithOffset(position, offset/2);

                recyclerViewCategories.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.layout_category_item).performClick();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        detector = new SimpleGestureFilter(this,this);

//        categoryList = new ArrayList<>(Arrays.asList(categories));
//        categoriesAdapter = new CategoriesAdapter(this, categoryList);




        /********************************************* User Page ******************************************/
        // Show all Posts of User in Recycler View
        userPostsAdapter = new UserPostsAdapter(userPostList);

        recyclerUserPosts = findViewById(R.id.recyclerUserPosts);
        recyclerUserPosts.setNestedScrollingEnabled(false);
        recyclerUserPosts.setLayoutManager(new LinearLayoutManager(this));
        recyclerUserPosts.setItemAnimator(new DefaultItemAnimator());
        recyclerUserPosts.setAdapter(userPostsAdapter);
        recyclerUserPosts.setFocusable(false);

        showMyPostsData();

        // Show all Follow/Followers in Recycler View
        followFollowerAdapter = new FollowFollowerAdapter(followsList);

        recyclerFollowFollower = findViewById(R.id.recycler_follow_follower_likes);
        recyclerFollowFollower.setNestedScrollingEnabled(false);
        recyclerFollowFollower.setLayoutManager(new LinearLayoutManager(this));
        recyclerFollowFollower.setItemAnimator(new DefaultItemAnimator());
        recyclerFollowFollower.setAdapter(followFollowerAdapter);
        recyclerFollowFollower.setFocusable(false);


        tvTitleFollowFollower = findViewById(R.id.title_follow);
        tvHiddenTitleFollowFollower = findViewById(R.id.title_hidden_follow);
        hiddenDividerFollowFollower = findViewById(R.id.hidden_divider_follow);



        drawerLayout = findViewById(R.id.drawer_user_page);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                hiddenDividerFollowFollower.setVisibility(View.INVISIBLE);
                tvHiddenTitleFollowFollower.setVisibility(View.INVISIBLE);

                if (drawerTitle != null) {
                    tvTitleFollowFollower.setText(drawerTitle);
                    tvHiddenTitleFollowFollower.setText(drawerTitle);
                }
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                followsList.clear();
                followFollowerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        tvFollow = findViewById(R.id.tv_follow_total);
        tvFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerTitle = "FOLLOW";
                showFollowsData();
                drawerLayout.openDrawer(Gravity.END);
            }
        });
        tvFollower = findViewById(R.id.tv_follower_total);
        tvFollower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerTitle = "FOLLOWER";
                showFollowsData();
                drawerLayout.openDrawer(Gravity.END);
            }
        });

        imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(Gravity.END);

            }
        });

        imgReportUser = findViewById(R.id.img_report_user);
        imgReportUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reportTitle = "Report User";
                String reportText = "Are you sure to report this user?";
                ModalReport modalReport = new ModalReport(v.getContext(), "user", 0, reportTitle, reportText);
            }
        });

        btnFollowUser = findViewById(R.id.btn_follow_user);
        btnFollowUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnFollowUser.getText().equals("Follow")) {
                    btnFollowUser.setBackgroundResource(R.drawable.round_corner_button_red);
                    btnFollowUser.setText("Unfollow");
                }
                else {
                    btnFollowUser.setBackgroundResource(R.drawable.round_corner_button_grey);
                    btnFollowUser.setText("Follow");
                }
            }
        });

        ///////////////////////// Animation while scrolling all Follow/Followers
        scrollViewFollowFollower = findViewById(R.id.scroll_follow_follower);
//        scrollView.smoothScrollTo(0, 0);
        scrollViewFollowFollower.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {

                Rect scrollBounds = new Rect();
                scrollViewFollowFollower.getHitRect(scrollBounds);
                if (tvTitleFollowFollower.getLocalVisibleRect(scrollBounds)) {
                    // if layout even a single pixel, is within the visible area do something
                    tvHiddenTitleFollowFollower.setVisibility(View.INVISIBLE);
                    hiddenDividerFollowFollower.setVisibility(View.INVISIBLE);

                } else {
                    // if layout goes out or scrolled out of the visible area do something
//                    Toast.makeText(MyPageActivity.this, "Title is out of focus!", Toast.LENGTH_SHORT).show();
                    tvHiddenTitleFollowFollower.setVisibility(View.VISIBLE);
                    hiddenDividerFollowFollower.setVisibility(View.VISIBLE);
                }

            }
        });


        /********** Show posts in Recycler View **************/
//        recyclerView = findViewById(R.id.recyclerPosts);
        /*recyclerView.setNestedScrollingEnabled(false);

        postsAdapter = new PostsAdapter(postList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(postsAdapter);

        showPostsData();*/

        /*ImageView imgLike = findViewById(R.id.img_favorite);
        imgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(HomeActivity.this, "Like button clicked", Toast.LENGTH_SHORT).show();
            }
        });*/




        //llPosts = findViewById(R.id.layout_posts);


        /********************** Bottom Navigation Bar ********************/
        BottomNavigationViewEx bottomNavigationViewEx = findViewById(R.id.bottom_navigation);
        bottomNavigationViewEx.enableAnimation(false);
        bottomNavigationViewEx.enableItemShiftingMode(false);
        bottomNavigationViewEx.enableShiftingMode(false);

        bottomNavigationViewEx.setIconSize(25f, 25f);
        bottomNavigationViewEx.setTextVisibility(false);

        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_home:
                        break;
                    case R.id.action_clipList:
                        Intent intentClipList = new Intent(getApplicationContext(), ClipListActivity.class);
                        startActivity(intentClipList);
                        break;
                    case R.id.action_search:
                        Intent intentSearch = new Intent(getApplicationContext(), SearchActivity.class);
//                        finishAffinity();
//                        intentSearch.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intentSearch);
                        break;
                    case R.id.action_notification:
                        Intent intentNotify = new Intent(getApplicationContext(), NotifyActivity.class);
                        startActivity(intentNotify);
                        break;
                    case R.id.action_myPage:
                        if (session.isLoggedIn()) {
                            Intent intentMyPage = new Intent(getApplicationContext(), MyPageActivity.class);
                            startActivity(intentMyPage);
                        }

                        else {
                            Intent intentMyPage = new Intent(getApplicationContext(), EnrollActivity.class);
                            startActivity(intentMyPage);
                        }
                        break;
                }
                return true;
            }
        });
    }

    private void showMyPostsData () {
        Post post1 = new Post();
        userPostList.add(post1);

        Post post2 = new Post();
        userPostList.add(post2);

        Post post3 = new Post();
        userPostList.add(post3);

        Post post4 = new Post();
        userPostList.add(post4);

        Post post5 = new Post();
        userPostList.add(post5);

        Post post6 = new Post();
        userPostList.add(post6);

        userPostsAdapter.notifyDataSetChanged();
    }

    private void showFollowsData () {
        Follow follow1 = new Follow();
        followsList.add(follow1);

        Follow follow2 = new Follow();
        followsList.add(follow2);

        Follow follow3 = new Follow();
        followsList.add(follow3);

        Follow follow4 = new Follow();
        followsList.add(follow4);

        Follow follow5 = new Follow();
        followsList.add(follow5);

        Follow follow6 = new Follow();
        followsList.add(follow6);

        Follow follow7 = new Follow();
        followsList.add(follow7);

        Follow follow8 = new Follow();
        followsList.add(follow8);

        Follow follow9 = new Follow();
        followsList.add(follow9);

        Follow follow10 = new Follow();
        followsList.add(follow10);

        Follow follow11 = new Follow();
        followsList.add(follow11);

        Follow follow12 = new Follow();
        followsList.add(follow12);

        Follow follow13 = new Follow();
        followsList.add(follow13);

        Follow follow14 = new Follow();
        followsList.add(follow14);

        Follow follow15 = new Follow();
        followsList.add(follow15);

        Follow follow16 = new Follow();
        followsList.add(follow16);

        Follow follow17 = new Follow();
        followsList.add(follow17);

        followFollowerAdapter.notifyDataSetChanged();
    }



    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            ScreenSlidePageFragment topPageFragment = new ScreenSlidePageFragment();
            Bundle args = new Bundle();
            args.putInt("categoryID", position + 1);
            topPageFragment.setArguments(args);

            return topPageFragment;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }



    private void CopyRAWtoSDCard(int id, String path) throws IOException {
        InputStream in = getResources().openRawResource(id);
        FileOutputStream out = new FileOutputStream(path);
        byte[] buff = new byte[1024];
        int read = 0;
        try {
            while ((read = in.read(buff)) > 0) {
                out.write(buff, 0, read);
            }
        } finally {
            in.close();
            out.close();
        }
    }

    /*
    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }
    */

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(0,0);
    }
}
