package com.dds.parade.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dds.parade.Models.LogInData;
import com.dds.parade.Models.LogInResponse;
import com.dds.parade.R;
import com.dds.parade.RetrofitHelper;
import com.dds.parade.RetrofitService;
import com.dds.parade.SessionManager;

import java.security.MessageDigest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {

    Button btnSignIn;

    EditText etEmail, etPassword;
    String messageLogIn;
    RetrofitService service;

    Context context;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        session = new SessionManager(this);

        context = this;
        service = RetrofitHelper.getRetrofitService();

        etEmail = findViewById(R.id.et_sign_in_email);
        etPassword = findViewById(R.id.et_sign_in_password);

        btnSignIn = findViewById(R.id.btn_go);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogin(etEmail.getText().toString(), etPassword.getText().toString());
                //If successfully signed in
            }
        });
    }

    public void userLogin(final String email, String password) {

        if (email.trim().length() <= 0 || password.trim().length() <=0) {
            //If email or password field is empty
            new AlertDialog.Builder(this)
                    .setMessage("Please fill up all the fields")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
        }

        else {
            String sha256Password = sha256(password);
            Log.e("Asif-LogIn", sha256Password);

            Call<LogInResponse> logInCall = service.logIn(email, sha256Password);
            logInCall.enqueue(new Callback<LogInResponse>() {
                @Override
                public void onResponse(Call<LogInResponse> call, Response<LogInResponse> response) {
                    String status = "";
                    LogInResponse logInResponse = response.body();
                    Log.e("Asif-LogInResponse","" + response.body() + " Response Code: " + response.code());

                    if(logInResponse != null) status = logInResponse.getStatus();

                    if (status.equals("success")) {
                        LogInData logInData = logInResponse.getData();
                        Log.e("Asif-LogIn", "Status: success, " + logInData.getSession());

                        session.createLoginSession(logInData.getSession(), email);

                        if (session.isLoggedIn()) {
                            Intent intentMyPage = new Intent(getApplicationContext(), MyPageActivity.class);
                            intentMyPage.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intentMyPage);
                            finish();
                        }
                    }
                    else if (status.equals("error")) {
                        LogInData logInData = logInResponse.getData();
                        new AlertDialog.Builder(context)
                                .setMessage(logInData.getErrorMessage().get(0))
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else {
                        new AlertDialog.Builder(context)
                                .setMessage("Invalid Email or Password!")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                }

                @Override
                public void onFailure(Call<LogInResponse> call, Throwable t) {
                    new AlertDialog.Builder(context)
                            .setMessage("Sorry!Could not connect to the server.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                }
            });

        }

    }

    public static String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
}
