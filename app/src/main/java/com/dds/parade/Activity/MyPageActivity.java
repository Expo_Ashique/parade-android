package com.dds.parade.Activity;

import android.content.Intent;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.dds.parade.Adapter.FavoritesAdapter;
import com.dds.parade.Adapter.FollowFollowerAdapter;
import com.dds.parade.Adapter.MyPostsAdapter;
import com.dds.parade.FullDrawerLayout;
import com.dds.parade.Models.Follow;
import com.dds.parade.Models.Post;
import com.dds.parade.Others.CustomRemoveAccountDialog;
import com.dds.parade.R;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;
import java.util.List;

public class MyPageActivity extends AppCompatActivity implements View.OnClickListener{
    List<Post> myPostList = new ArrayList<>();
    RecyclerView recyclerMyPosts;
    MyPostsAdapter myPostsAdapter;

    ScrollView scrollView;
    TextView tvTitle, tvHiddenTitle;
    View hiddenDivider;

    ScrollView scrollViewFollowFollower;
    TextView tvTitleFollowFollower, tvHiddenTitleFollowFollower;
    View hiddenDividerFollowFollower;

    String drawerTitle;

    List<Follow> followsList = new ArrayList<>();
    List<Post> favoritePostsList = new ArrayList<>();
    RecyclerView recyclerSideBar;
    FollowFollowerAdapter followFollowerAdapter;
    FavoritesAdapter favoritesAdapter;

    LinearLayout tvFollow, tvFollower, tvFavorites;

    ImageView imgBack, imgBackSettings;
    Button btnEdit;

    LinearLayout llSettingsPage;

    RelativeLayout rlChangePassword, rlRemoveAccount;

    FullDrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_page);

        llSettingsPage = findViewById(R.id.layout_settings_page);

        /************** Show all Posts of mine in Recycler View  ******************/
        myPostsAdapter = new MyPostsAdapter(myPostList);

        recyclerMyPosts = findViewById(R.id.recyclerMyPosts);
        recyclerMyPosts.setNestedScrollingEnabled(false);
        recyclerMyPosts.setLayoutManager(new LinearLayoutManager(this));
        recyclerMyPosts.setItemAnimator(new DefaultItemAnimator());
        recyclerMyPosts.setAdapter(myPostsAdapter);
        recyclerMyPosts.setFocusable(false);

        showMyPostsData();

        /************** Show all Follow/Followers in Recycler View  ******************/
        followFollowerAdapter = new FollowFollowerAdapter(followsList);
        favoritesAdapter = new FavoritesAdapter(favoritePostsList);

        recyclerSideBar = findViewById(R.id.recycler_follow_follower_likes);
        recyclerSideBar.setNestedScrollingEnabled(false);
        recyclerSideBar.setLayoutManager(new LinearLayoutManager(this));
        recyclerSideBar.setItemAnimator(new DefaultItemAnimator());
        recyclerSideBar.setFocusable(false);



        tvTitle = findViewById(R.id.title_my_page);
        tvHiddenTitle = findViewById(R.id.title_hidden);
        hiddenDivider = findViewById(R.id.hidden_divider);

        tvTitleFollowFollower = findViewById(R.id.title_follow);
        tvHiddenTitleFollowFollower = findViewById(R.id.title_hidden_follow);
        hiddenDividerFollowFollower = findViewById(R.id.hidden_divider_follow);



        drawerLayout = findViewById(R.id.drawer);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                hiddenDividerFollowFollower.setVisibility(View.INVISIBLE);
                tvHiddenTitleFollowFollower.setVisibility(View.INVISIBLE);

                if (drawerTitle != null) {
                    tvTitleFollowFollower.setText(drawerTitle);
                    tvHiddenTitleFollowFollower.setText(drawerTitle);
                }
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {


            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                followsList.clear();
                followFollowerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        tvFollow = findViewById(R.id.layout_follow);
        tvFollow.setOnClickListener(this);

        tvFollower = findViewById(R.id.layout_follower);
        tvFollower.setOnClickListener(this);

        tvFavorites = findViewById(R.id.layout_likes);
        tvFavorites.setOnClickListener(this);

        imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(this);

        imgBackSettings = findViewById(R.id.img_back_settings);
        imgBackSettings.setOnClickListener(this);

        btnEdit = findViewById(R.id.btn_edit);
        btnEdit.setOnClickListener(this);

        rlChangePassword = findViewById(R.id.container_change_password);
        rlChangePassword.setOnClickListener(this);

        rlRemoveAccount = findViewById(R.id.container_remove_account);
        rlRemoveAccount.setOnClickListener(this);


        /******************** Animation while scrolling all Posts *******************************/
        scrollView = findViewById(R.id.scroll_my_page_content);
//        scrollView.smoothScrollTo(0, 0);
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {

                Rect scrollBounds = new Rect();
                scrollView.getHitRect(scrollBounds);
                if (tvTitle.getLocalVisibleRect(scrollBounds)) {
                    // if layout even a single pixel, is within the visible area do something
                    tvHiddenTitle.setVisibility(View.INVISIBLE);
                    hiddenDivider.setVisibility(View.INVISIBLE);

                } else {
                    // if layout goes out or scrolled out of the visible area do something
//                    Toast.makeText(MyPageActivity.this, "Title is out of focus!", Toast.LENGTH_SHORT).show();
                    tvHiddenTitle.setVisibility(View.VISIBLE);
                    hiddenDivider.setVisibility(View.VISIBLE);
                }

            }
        });


        /******************** Animation while scrolling all Follow/Followers *******************************/
        scrollViewFollowFollower = findViewById(R.id.scroll_follow_follower);
//        scrollView.smoothScrollTo(0, 0);
        scrollViewFollowFollower.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {

                Rect scrollBounds = new Rect();
                scrollViewFollowFollower.getHitRect(scrollBounds);
                if (tvTitleFollowFollower.getLocalVisibleRect(scrollBounds)) {
                    // if layout even a single pixel, is within the visible area do something
                    tvHiddenTitleFollowFollower.setVisibility(View.INVISIBLE);
                    hiddenDividerFollowFollower.setVisibility(View.INVISIBLE);

                } else {
                    // if layout goes out or scrolled out of the visible area do something
//                    Toast.makeText(MyPageActivity.this, "Title is out of focus!", Toast.LENGTH_SHORT).show();
                    tvHiddenTitleFollowFollower.setVisibility(View.VISIBLE);
                    hiddenDividerFollowFollower.setVisibility(View.VISIBLE);
                }

            }
        });

        /********************** Bottom Navigation Bar ********************/

        BottomNavigationViewEx bottomNavigationViewEx = findViewById(R.id.bottom_navigation);
        bottomNavigationViewEx.enableAnimation(false);
        bottomNavigationViewEx.enableItemShiftingMode(false);
        bottomNavigationViewEx.enableShiftingMode(false);
        bottomNavigationViewEx.setSelectedItemId(R.id.action_myPage);
        bottomNavigationViewEx.setIconSize(25f, 25f);
        bottomNavigationViewEx.setTextVisibility(false);

        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_home:
                        Intent intentHome = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(intentHome);
                        break;
                    case R.id.action_clipList:
                        Intent intentClipList = new Intent(getApplicationContext(), ClipListActivity.class);
//                        intentClipList.setFlags(intentClipList.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intentClipList);
                        break;
                    case R.id.action_search:
                        Intent intentSearch = new Intent(getApplicationContext(), SearchActivity.class);
                        startActivity(intentSearch);
                        break;
                    case R.id.action_notification:
                        Intent intentNotify = new Intent(getApplicationContext(), NotifyActivity.class);
                        startActivity(intentNotify);
                        break;
                    case R.id.action_myPage:
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.layout_follow:
                drawerTitle = "FOLLOW";
                recyclerSideBar.setAdapter(followFollowerAdapter);
                showFollowsData();
                drawerLayout.openDrawer(Gravity.END);
                break;

            case R.id.layout_follower:
                drawerTitle = "FOLLOWER";
                recyclerSideBar.setAdapter(followFollowerAdapter);
                showFollowsData();
                drawerLayout.openDrawer(Gravity.END);
                break;

            case R.id.layout_likes:
                drawerTitle = "LIKES";
                recyclerSideBar.setAdapter(favoritesAdapter);
                showFavoritesData();
                drawerLayout.openDrawer(Gravity.END);
                break;

            case R.id.img_back:
                drawerLayout.closeDrawer(Gravity.END);
                break;

            case R.id.btn_edit:
                Animation animSlideLeft = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_left);

                if (llSettingsPage != null) {
                    if (llSettingsPage.getVisibility() == View.GONE) {
                        llSettingsPage.setAnimation(animSlideLeft);
                        llSettingsPage.setVisibility(View.VISIBLE);
                    }
                }
                break;

            case R.id.container_change_password:
                Intent intentChangePassword = new Intent(this, ChangePasswordActivity.class);
                startActivity(intentChangePassword);
                break;

            case R.id.container_remove_account:
                CustomRemoveAccountDialog dialog = new CustomRemoveAccountDialog(v.getContext());
                dialog.show();
                break;

            case R.id.img_back_settings:
                Animation animSlideRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_right);

                if (llSettingsPage != null) {
                    if (llSettingsPage.getVisibility() == View.VISIBLE) {
                        llSettingsPage.setAnimation(animSlideRight);
                        llSettingsPage.setVisibility(View.GONE);
                    }
                }
                break;

        }
    }

    private void showMyPostsData () {
        Post post1 = new Post();
        myPostList.add(post1);

        Post post2 = new Post();
        myPostList.add(post2);

        Post post3 = new Post();
        myPostList.add(post3);

        Post post4 = new Post();
        myPostList.add(post4);

        Post post5 = new Post();
        myPostList.add(post5);

        Post post6 = new Post();
        myPostList.add(post6);

        myPostsAdapter.notifyDataSetChanged();
    }

    private void showFollowsData () {
        Follow follow1 = new Follow();
        followsList.add(follow1);

        Follow follow2 = new Follow();
        followsList.add(follow2);

        Follow follow3 = new Follow();
        followsList.add(follow3);

        Follow follow4 = new Follow();
        followsList.add(follow4);

        Follow follow5 = new Follow();
        followsList.add(follow5);

        Follow follow6 = new Follow();
        followsList.add(follow6);

        Follow follow7 = new Follow();
        followsList.add(follow7);

        Follow follow8 = new Follow();
        followsList.add(follow8);

        Follow follow9 = new Follow();
        followsList.add(follow9);

        Follow follow10 = new Follow();
        followsList.add(follow10);

        Follow follow11 = new Follow();
        followsList.add(follow11);

        Follow follow12 = new Follow();
        followsList.add(follow12);

        Follow follow13 = new Follow();
        followsList.add(follow13);

        Follow follow14 = new Follow();
        followsList.add(follow14);

        Follow follow15 = new Follow();
        followsList.add(follow15);

        Follow follow16 = new Follow();
        followsList.add(follow16);

        Follow follow17 = new Follow();
        followsList.add(follow17);

        followFollowerAdapter.notifyDataSetChanged();
    }

    public void showFavoritesData () {
        Post post1 = new Post();
        favoritePostsList.add(post1);

        Post post2 = new Post();
        favoritePostsList.add(post2);

        Post post3 = new Post();
        favoritePostsList.add(post3);

        Post post4 = new Post();
        favoritePostsList.add(post4);

        favoritesAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(0,0);
    }


}
