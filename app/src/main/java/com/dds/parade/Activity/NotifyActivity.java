package com.dds.parade.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.dds.parade.Adapter.NotificationsAdapter;
import com.dds.parade.Models.Notification;
import com.dds.parade.R;
import com.dds.parade.SessionManager;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;
import java.util.List;

public class NotifyActivity extends AppCompatActivity {
    List<Notification> notificationsList = new ArrayList<>();
    RecyclerView recyclerView;
    NotificationsAdapter notificationsAdapter;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify);

        session = new SessionManager(this);
//        ImageView imageView = findViewById(R.id.imgProfile);
//        Bitmap bm = BitmapFactory.decodeResource(getResources(),R.drawable.user);
//        RoundImage roundedImage = new RoundImage(bm);
//        imageView.setImageDrawable(roundedImage);

        recyclerView = findViewById(R.id.recyclerNotify);
        recyclerView.setNestedScrollingEnabled(false);

        notificationsAdapter = new NotificationsAdapter(notificationsList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(notificationsAdapter);

        showNotificationsData();

        /*Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.user);
        Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 100);

        ImageView circularImageView = (ImageView)findViewById(R.id.imageView);
        circularImageView.setImageBitmap(circularBitmap);*/

        /********************** Bottom Navigation Bar ********************/

        BottomNavigationViewEx bottomNavigationViewEx = findViewById(R.id.bottom_navigation);
        bottomNavigationViewEx.enableAnimation(false);
        bottomNavigationViewEx.enableItemShiftingMode(false);
        bottomNavigationViewEx.enableShiftingMode(false);
        bottomNavigationViewEx.setSelectedItemId(R.id.action_notification);
        bottomNavigationViewEx.setIconSize(25f, 25f);
        bottomNavigationViewEx.setTextVisibility(false);

        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_home:
                        Intent intentHome = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(intentHome);
                        break;
                    case R.id.action_search:
                        Intent intentSearch = new Intent(getApplicationContext(), SearchActivity.class);
                        startActivity(intentSearch);
                        break;
                    case R.id.action_clipList:
                        Intent intentClipList = new Intent(getApplicationContext(), ClipListActivity.class);
                        startActivity(intentClipList);
                    case R.id.action_notification:
                        break;
                    case R.id.action_myPage:
                        if (session.isLoggedIn()) {
                            Intent intentMyPage = new Intent(getApplicationContext(), MyPageActivity.class);
                            startActivity(intentMyPage);
                        }

                        else {
                            Intent intentMyPage = new Intent(getApplicationContext(), EnrollActivity.class);
                            startActivity(intentMyPage);
                        }
                        break;
                }
                return true;
            }
        });
    }

    private void showNotificationsData () {
        Notification notification1 = new Notification("img/preview", "USER:xxxxxxxx & USER:xxx commented on your post", "This is an awesome pic. A beautiful place indeed. Hope you will upload more this kind of stuff. Farewell to you");
        notificationsList.add(notification1);

        Notification notification2 = new Notification("img/preview", "USER:xxxxxxxx & USER:xxx commented on your post", "This is an awesome pic. A beautiful place indeed. Hope you will upload more this kind of stuff. Farewell to you");
        notificationsList.add(notification2);

        Notification notification3 = new Notification("img/preview", "USER:xxxxxxxx & USER:xxx commented on your post", "This is an awesome pic. A beautiful place indeed. Hope you will upload more this kind of stuff. Farewell to you");
        notificationsList.add(notification3);

        Notification notification4 = new Notification("img/preview", "USER:xxxxxxxx & USER:xxx commented on your post", "This is an awesome pic. A beautiful place indeed. Hope you will upload more this kind of stuff. Farewell to you");
        notificationsList.add(notification4);

        Notification notification5 = new Notification("img/preview", "USER:xxxxxxxx & USER:xxx commented on your post", "This is an awesome pic. A beautiful place indeed. A beautiful place indeed. Hope you will upload more this kind of stuff. Farewell to you");
        notificationsList.add(notification5);

        Notification notification6 = new Notification("img/preview", "USER:xxxxxxxx & USER:xxx commented on your post", "This is an awesome pic. A beautiful place indeed. Hope you will upload more this kind of stuff. Farewell to you");
        notificationsList.add(notification6);

        Notification notification7 = new Notification("img/preview", "USER:xxxxxxxx & USER:xxx commented on your post", "This is an awesome pic. A beautiful place indeed. Hope you will upload more this kind of stuff. Farewell to you");
        notificationsList.add(notification7);

        Notification notification8 = new Notification("img/preview", "USER:xxxxxxxx & USER:xxx commented on your post", "This is an awesome pic. A beautiful place indeed. Hope you will upload more this kind of stuff. Farewell to you");
        notificationsList.add(notification8);




        notificationsAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.no_change,R.anim.no_change);
    }
}
