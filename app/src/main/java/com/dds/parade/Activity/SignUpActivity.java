package com.dds.parade.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dds.parade.R;

public class SignUpActivity extends AppCompatActivity {

    RelativeLayout sectorName;
    RelativeLayout sectorEmail;
    RelativeLayout sectorPassword;
    RelativeLayout sectorSignUp;
    Button btnNextName, btnNextEmail, btnNextPassword, btnSignUp;
    HorizontalScrollView scrollView;
    TextView tvSectorTitle;
    View viewIndicator1;
    View viewIndicator2;
    View viewIndicator3;
    View viewIndicator4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        //find the width of the screen and set the width of scrollview with it
        int screenWidth = getResources().getDisplayMetrics().widthPixels;

        sectorName = findViewById(R.id.sector_username);
        sectorName.setLayoutParams(new LinearLayout.LayoutParams(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
        sectorEmail = findViewById(R.id.sector_email);
        sectorEmail.setLayoutParams(new LinearLayout.LayoutParams(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
        sectorPassword = findViewById(R.id.sector_password);
        sectorPassword.setLayoutParams(new LinearLayout.LayoutParams(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
        sectorSignUp = findViewById(R.id.sector_btn_sign_up);
        sectorSignUp.setLayoutParams(new LinearLayout.LayoutParams(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

        scrollView = findViewById(R.id.scroll_sign_up);
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        tvSectorTitle = findViewById(R.id.sector_title);
        viewIndicator1 = findViewById(R.id.indicator_field_1);
        viewIndicator2 = findViewById(R.id.indicator_field_2);
        viewIndicator3 = findViewById(R.id.indicator_field_3);
        viewIndicator4 = findViewById(R.id.indicator_field_4);

        btnNextName = findViewById(R.id.btn_next_name);
        btnNextName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.scrollTo(sectorEmail.getLeft(), 0);
                tvSectorTitle.setText("EMAIL");
                viewIndicator1.setBackgroundResource(R.drawable.circle_solid_grey);
                viewIndicator2.setBackgroundResource(R.drawable.circle_solid_red);
            }
        });


        btnNextEmail = findViewById(R.id.btn_next_email);
        btnNextEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.scrollTo(sectorPassword.getLeft(), 0);
                tvSectorTitle.setText("Password");
                viewIndicator2.setBackgroundResource(R.drawable.circle_solid_grey);
                viewIndicator3.setBackgroundResource(R.drawable.circle_solid_red);
            }
        });

        btnNextPassword = findViewById(R.id.btn_next_password);
        btnNextPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.scrollTo(sectorSignUp.getLeft(), 0);
                tvSectorTitle.setText("FINISH");
                viewIndicator3.setBackgroundResource(R.drawable.circle_solid_grey);
                viewIndicator4.setBackgroundResource(R.drawable.circle_solid_red);
            }
        });

    }
}
