package com.dds.parade.Activity;

import android.content.Intent;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.dds.parade.Adapter.SearchedPostsAdapter;
import com.dds.parade.FullDrawerLayout;
import com.dds.parade.Models.Post;
import com.dds.parade.R;
import com.dds.parade.SessionManager;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {
    AutoCompleteTextView inputSearch;
    FullDrawerLayout drawerLayout;

    SessionManager session;

    List<Post> searchedPostList = new ArrayList<>();
    RecyclerView recyclerSearchedPosts;
    SearchedPostsAdapter searchedPostsAdapter;

    ScrollView scrollView;
    TextView tvTitle;
    TextView tvHiddenTitle;
    View hiddenDivider;

    String searchKey;

    ListView listPopularSearch;
    String[] valuesPopularSearch = new String[]{
            "Fashion",
            "Landscapes",
            "Personal"
    };

    ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        session = new SessionManager(this);

        listPopularSearch = findViewById(R.id.list_popular_search);

        ArrayAdapter<String> adapterSearch = new ArrayAdapter<String>(this,
                R.layout.row_popular_search_item,R.id.list_items, valuesPopularSearch);

        listPopularSearch.setAdapter(adapterSearch);
        listPopularSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(SearchActivity.this,"" + valuesPopularSearch[position], Toast.LENGTH_SHORT).show();
                searchKey = valuesPopularSearch[position];
                drawerLayout.openDrawer(Gravity.END);
            }
        });

        /************** Show all Posts of mine in Recycler View  ******************/
        searchedPostsAdapter = new SearchedPostsAdapter(searchedPostList);

        recyclerSearchedPosts = findViewById(R.id.recycler_search_result);
        recyclerSearchedPosts.setNestedScrollingEnabled(false);
        recyclerSearchedPosts.setLayoutManager(new LinearLayoutManager(this));
        recyclerSearchedPosts.setItemAnimator(new DefaultItemAnimator());
        recyclerSearchedPosts.setAdapter(searchedPostsAdapter);
        recyclerSearchedPosts.setFocusable(false);

        showSearchedPostsData();

        tvTitle = findViewById(R.id.title_search);
        tvHiddenTitle = findViewById(R.id.title_hidden_search);
        hiddenDivider = findViewById(R.id.hidden_divider_search);


        drawerLayout = findViewById(R.id.layout_drawer_search_page);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                tvHiddenTitle.setVisibility(View.INVISIBLE);
                hiddenDivider.setVisibility(View.INVISIBLE);

                if (searchKey != null) {
                    tvTitle.setText(searchKey);
                    tvHiddenTitle.setText(searchKey);
                }
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });




        inputSearch = findViewById(R.id.input_search_text);
        inputSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    Intent intentSearchResult = new Intent(v.getContext(), SearchResultActivity.class);
//                    intentSearchResult.putExtra("SearchKey",  "" + inputSearch.getText());
//                    startActivity(intentSearchResult);
                    InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    searchKey = inputSearch.getText().toString();
                    drawerLayout.openDrawer(Gravity.END);
                    return true;
                }
                return false;
            }
        });


        /******************** Animation while scrolling all Posts *******************************/
        scrollView = findViewById(R.id.scroll_search_result);
//        scrollView.smoothScrollTo(0, 0);
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {

                Rect scrollBounds = new Rect();
                scrollView.getHitRect(scrollBounds);
                if (tvTitle.getLocalVisibleRect(scrollBounds)) {
                    // if layout even a single pixel, is within the visible area do something
                    tvHiddenTitle.setVisibility(View.INVISIBLE);
                    hiddenDivider.setVisibility(View.INVISIBLE);

                } else {
                    // if layout goes out or scrolled out of the visible area do something
//                    Toast.makeText(MyPageActivity.this, "Title is out of focus!", Toast.LENGTH_SHORT).show();
                    tvHiddenTitle.setVisibility(View.VISIBLE);
                    hiddenDivider.setVisibility(View.VISIBLE);
                }

            }
        });




        imgBack = findViewById(R.id.img_back_search);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(Gravity.END);
            }
        });



        /********************** Bottom Navigation Bar ********************/

        BottomNavigationViewEx bottomNavigationViewEx = findViewById(R.id.bottom_navigation);
        bottomNavigationViewEx.enableAnimation(false);
        bottomNavigationViewEx.enableItemShiftingMode(false);
        bottomNavigationViewEx.enableShiftingMode(false);
        bottomNavigationViewEx.setSelectedItemId(R.id.action_search);
        bottomNavigationViewEx.setIconSize(25f, 25f);
        bottomNavigationViewEx.setTextVisibility(false);

        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_home:
                        Intent intentHome = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(intentHome);
                        break;
                    case R.id.action_clipList:
                        Intent intentClipList = new Intent(getApplicationContext(), ClipListActivity.class);
                        startActivity(intentClipList);
                        break;
                    case R.id.action_notification:
                        Intent intentNotify = new Intent(getApplicationContext(), NotifyActivity.class);
                        startActivity(intentNotify);
                        break;
                    case R.id.action_myPage:
                        if (session.isLoggedIn()) {
                            Intent intentMyPage = new Intent(getApplicationContext(), MyPageActivity.class);
                            startActivity(intentMyPage);
                        }

                        else {
                            Intent intentMyPage = new Intent(getApplicationContext(), EnrollActivity.class);
                            startActivity(intentMyPage);
                        }
                        break;
                }
                return true;
            }
        });
    }

    private void showSearchedPostsData () {
        Post post1 = new Post();
        searchedPostList.add(post1);

        Post post2 = new Post();
        searchedPostList.add(post2);

        Post post3 = new Post();
        searchedPostList.add(post3);

        Post post4 = new Post();
        searchedPostList.add(post4);

        Post post5 = new Post();
        searchedPostList.add(post5);

        Post post6 = new Post();
        searchedPostList.add(post6);

        searchedPostsAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(0,0);
    }
}
