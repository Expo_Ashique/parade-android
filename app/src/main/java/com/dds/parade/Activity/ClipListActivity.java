package com.dds.parade.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.ImageView;

import com.dds.parade.Adapter.ClipsAdapter;
import com.dds.parade.Models.Clip;
import com.dds.parade.R;
import com.dds.parade.SessionManager;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;
import java.util.List;

public class ClipListActivity extends AppCompatActivity {

    List<Clip> clipList = new ArrayList<>();
    RecyclerView recyclerView;
    ClipsAdapter clipsAdapter;
    ImageView imageView1;

    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clips);


        session = new SessionManager(this);

        recyclerView = findViewById(R.id.recyclerClips);
        recyclerView.setNestedScrollingEnabled(false);

        clipsAdapter = new ClipsAdapter(clipList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(clipsAdapter);

        showClipsData();


        /*ImageView mimageView = findViewById(R.id.image_preview_1);

        Bitmap mbitmap = ((BitmapDrawable) getResources().getDrawable(R.drawable.japan1)).getBitmap();
        Bitmap imageRounded = Bitmap.createBitmap(mbitmap.getWidth(), mbitmap.getHeight(), mbitmap.getConfig());
        Canvas canvas = new Canvas(imageRounded);
        Paint mpaint = new Paint();
        mpaint.setAntiAlias(true);
        mpaint.setShader(new BitmapShader(mbitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
        canvas.drawRoundRect((new RectF(0, 0, mbitmap.getWidth(), mbitmap.getHeight())), 100, 100, mpaint);// Round Image Corner 100 100 100 100
        mimageView.setImageBitmap(imageRounded);*/


        /********************** Bottom Navigation Bar ********************/

        BottomNavigationViewEx bottomNavigationViewEx = findViewById(R.id.bottom_navigation);
        bottomNavigationViewEx.enableAnimation(false);
        bottomNavigationViewEx.enableItemShiftingMode(false);
        bottomNavigationViewEx.enableShiftingMode(false);

        bottomNavigationViewEx.setIconSize(25f, 25f);
        bottomNavigationViewEx.setTextVisibility(false);
        bottomNavigationViewEx.setSelectedItemId(R.id.action_clipList);

        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_home:
                        Intent intentHome = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(intentHome);
                        break;
                    case R.id.action_clipList:
                        break;
                    case R.id.action_search:
                        Intent intentSearch = new Intent(getApplicationContext(), SearchActivity.class);
                        startActivity(intentSearch);
                        break;
                    case R.id.action_notification:
                        Intent intentNotify = new Intent(getApplicationContext(), NotifyActivity.class);
                        startActivity(intentNotify);
                        break;
                    case R.id.action_myPage:
                        if (session.isLoggedIn()) {
                            Intent intentMyPage = new Intent(getApplicationContext(), MyPageActivity.class);
                            startActivity(intentMyPage);
                        }

                        else {
                            Intent intentMyPage = new Intent(getApplicationContext(), EnrollActivity.class);
                            startActivity(intentMyPage);
                        }
                        break;
                }
                return true;
            }
        });

    }

    private void showClipsData () {
        Clip clip1 = new Clip("Kyoto Trip", "12", "");
        clipList.add(clip1);

        Clip clip2 = new Clip("Kyoto Trip", "12", "");
        clipList.add(clip2);

        Clip clip3 = new Clip("Kyoto Trip", "12", "");
        clipList.add(clip3);

        Clip clip4 = new Clip("Kyoto Trip", "12", "");
        clipList.add(clip4);

        clipsAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(0,0);
    }
}
