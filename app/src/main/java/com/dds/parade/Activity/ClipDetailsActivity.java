package com.dds.parade.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;

import com.dds.parade.Adapter.CardsAdapter;
import com.dds.parade.Models.Card;
import com.dds.parade.R;

import java.util.ArrayList;
import java.util.List;

public class ClipDetailsActivity extends AppCompatActivity implements View.OnClickListener{

    ImageView imgBackClipDetails;

    List<Card> cardList = new ArrayList<>();
    RecyclerView recyclerClipItems;
    CardsAdapter cardsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clip_details);

        imgBackClipDetails = findViewById(R.id.img_back_clip_details);
        imgBackClipDetails.setOnClickListener(this);

        recyclerClipItems = findViewById(R.id.recycler_clip_items);
//        recyclerClipItems.setHasFixedSize(true);
        cardsAdapter = new CardsAdapter(cardList);
        recyclerClipItems.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        recyclerClipItems.setItemAnimator(new DefaultItemAnimator());
        recyclerClipItems.setAdapter(cardsAdapter);

        showCardsData();

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.img_back_clip_details:
                Intent intentClipList = new Intent(getApplicationContext(), ClipListActivity.class);
                startActivity(intentClipList);
                finish();
                break;
        }
    }

    public void showCardsData() {
        Card card1 = new Card("image", "japan1", "Kyoto Trip");
        cardList.add(card1);

        Card card2 = new Card("image", "japan2","This text should be small, unfortunately it became  very big");
        cardList.add(card2);

        Card card3 = new Card("image", "japan1","This is a very large text");
        cardList.add(card3);

        Card card4 = new Card("image", "japan2","This text should be small, unfortunately it became  very big");
        cardList.add(card4);

        Card card5 = new Card("video", "japan1","Lorem Ipsum is simply dummy text of the printing and typesetting industry.");
        cardList.add(card5);

        Card card6 = new Card("image", "japan1","This is a very large text");
        cardList.add(card6);

        cardsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
    }
}
