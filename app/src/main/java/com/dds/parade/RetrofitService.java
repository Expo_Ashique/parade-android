package com.dds.parade;

import com.dds.parade.Models.Category;
import com.dds.parade.Models.CategoryPostsResponse;
import com.dds.parade.Models.LogInResponse;
import com.dds.parade.Models.Report;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitService {

//    @Headers("Authorization: 1am9x5qjcks2hnacg64q0b196s1d34m2")
    @GET("get-category-list/")
    Call<Category> getAllCategories();

    @GET("timeline/category/{categoryID}/")
    Call<CategoryPostsResponse> getPostsOfACategory(
            @Path("categoryID") int categoryID,
            @Query("session") String session,
            @Query("page") int pageNo
    );

    @GET("all-category-post/")
    Call<CategoryPostsResponse> getAllCategoryPosts(
            @Query("session") String session,
            @Query("number_of_post") int noOfPost,
            @Query("page") int pageNo
    );

    @FormUrlEncoded
    @POST("report-post/")
    Call<Report> submitReport (
            @Field("post_id") int postID,
            @Field("session") String session,
            @Field("is_report") int isReport,
            @Field("description") String description,
            @Field("status") String status
    );

    @FormUrlEncoded
    @POST("login/")
    Call<LogInResponse> logIn (
            @Field("email") String email,
            @Field("password") String password
    );

}
