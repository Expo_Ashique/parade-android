package com.dds.parade.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryPostsData {
    @SerializedName("timeline_data")
    @Expose
    private List<CategoryPostsTimelineData> timelineData = null;
    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("total_count")
    @Expose
    private Integer totalCount;

    public List<CategoryPostsTimelineData> getTimelineData() {
        return timelineData;
    }

    public void setTimelineData(List<CategoryPostsTimelineData> timelineData) {
        this.timelineData = timelineData;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }
}
