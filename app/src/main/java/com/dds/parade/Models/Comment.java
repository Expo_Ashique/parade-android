package com.dds.parade.Models;

import java.util.Date;

public class Comment {
    int id;
    int userId;
    Date time;
    String message;

    public Comment() {
    }

    public Comment(int id, int userId, Date time, String message) {
        this.id = id;
        this.userId = userId;
        this.time = time;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
