package com.dds.parade.Models;

public class Notification {

    private int id;
    private String userImgURL;
    private String title;
    private String description;

    public Notification(int id, String userImgURL, String title, String description) {
        this.id = id;
        this.userImgURL = userImgURL;
        this.title = title;
        this.description = description;
    }

    public Notification(String userImgURL, String title, String description) {
        this.userImgURL = userImgURL;
        this.title = title;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserImgURL() {
        return userImgURL;
    }

    public void setUserImgURL(String userImgURL) {
        this.userImgURL = userImgURL;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
