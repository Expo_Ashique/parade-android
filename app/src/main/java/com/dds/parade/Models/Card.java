package com.dds.parade.Models;

public class Card {
    int cardId;
    String type;
    String src;
    String title;

    public Card() {
    }

    public Card(String type, String src, String title) {
        this.type = type;
        this.src = src;
        this.title = title;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
