package com.dds.parade.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryPostsResponse {
    @SerializedName("data")
    @Expose
    private CategoryPostsData data;
    @SerializedName("message")
    @Expose
    private String message;
//    @SerializedName("status")
//    @Expose
//    private String status;

    public CategoryPostsData getData() {
        return data;
    }

    public void setData(CategoryPostsData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }

}
