package com.dds.parade.Models;

public class Post {
    private int id;
    private String img;
    private String title;
    private int likes;
    private int comments;

    public Post(int id, String img, String title, int likes, int comments) {
        this.id = id;
        this.img = img;
        this.title = title;
        this.likes = likes;
        this.comments = comments;
    }

    public Post(String img, String title, int likes, int comments) {
        this.img = img;
        this.title = title;
        this.likes = likes;
        this.comments = comments;
    }

    public Post() {
        this.img = img;
        this.title = title;
        this.likes = likes;
        this.comments = comments;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getComments() {
        return comments;
    }

    public void setComments(int comments) {
        this.comments = comments;
    }
}
