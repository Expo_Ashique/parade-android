package com.dds.parade.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryPostsTimelineData {

    @SerializedName("post_user_profile_pic")
    @Expose
    private String postUserProfilePic;
    @SerializedName("post_username")
    @Expose
    private String postUsername;
    @SerializedName("post_title")
    @Expose
    private String postTitle;
    @SerializedName("post_privacy")
    @Expose
    private String postPrivacy;
    @SerializedName("is_favorite")
    @Expose
    private Boolean isFavorite;
    @SerializedName("total_favorite")
    @Expose
    private Integer totalFavorite;
    @SerializedName("total_comment")
    @Expose
    private Integer totalComment;
    @SerializedName("post_card_type")
    @Expose
    private String postCardType;
    @SerializedName("added_date")
    @Expose
    private String addedDate;
    @SerializedName("post_user_id")
    @Expose
    private Integer postUserId;
    @SerializedName("post_id")
    @Expose
    private Integer postId;
    @SerializedName("post_card")
    @Expose
    private String postCard;

    public String getPostUserProfilePic() {
        return postUserProfilePic;
    }

    public void setPostUserProfilePic(String postUserProfilePic) {
        this.postUserProfilePic = postUserProfilePic;
    }

    public String getPostUsername() {
        return postUsername;
    }

    public void setPostUsername(String postUsername) {
        this.postUsername = postUsername;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostPrivacy() {
        return postPrivacy;
    }

    public void setPostPrivacy(String postPrivacy) {
        this.postPrivacy = postPrivacy;
    }

    public Boolean getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(Boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public Integer getTotalFavorite() {
        return totalFavorite;
    }

    public void setTotalFavorite(Integer totalFavorite) {
        this.totalFavorite = totalFavorite;
    }

    public Integer getTotalComment() {
        return totalComment;
    }

    public void setTotalComment(Integer totalComment) {
        this.totalComment = totalComment;
    }

    public String getPostCardType() {
        return postCardType;
    }

    public void setPostCardType(String postCardType) {
        this.postCardType = postCardType;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public Integer getPostUserId() {
        return postUserId;
    }

    public void setPostUserId(Integer postUserId) {
        this.postUserId = postUserId;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getPostCard() {
        return postCard;
    }

    public void setPostCard(String postCard) {
        this.postCard = postCard;
    }

}
