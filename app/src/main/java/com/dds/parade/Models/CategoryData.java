package com.dds.parade.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryData {
    @SerializedName("result")
    @Expose
    private List<CategoryResult> result = null;

    public List<CategoryResult> getResult() {
        return result;
    }

    public void setResult(List<CategoryResult> result) {
        this.result = result;
    }
}
